# Disco Player

Sound and music player for home automation system

## Description

Temat pracy dotyczy zaprojektowania i zbudowania prototypu odtwarzacza plików
dźwiękowych, w tym muzyki, w formatach WAV, MP3 i FLAC. Odtwarzacz powinien być
opracowany pod kątem integracji z systemami automatyki inteligentnego domu, jako
urządzenie na stałe współpracujące z infrastrukturą sieciową budynku.
Urządzenie, oprócz typowych walorów użytkowych, w tym intuicyjnego GUI z
zarządzaniem listą odtwarzania, powinno mieć także możliwość odtwarzania
komunikatów dźwiękowych. Komunikaty dźwiękowe mogą być zgłaszane m.in. przez
żądania od innych urządzeń w sieci lokalnej. Przez sieć lokalną powinna także
istnieć możliwość odczytu stanu urządzenia.

## Plan

- [x] Basic sanity check
- [x] Initial project
- [x] Include and check GUI library
- [x] Implement playing basic audio
- [x] Create basic playback GUI
- [x] Local network connection
- [x] Basic HTTP server
- [x] Web UI
- [x] REST API
- [x] MP3 and FLAC playback
