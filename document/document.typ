#import "codly.typ": *

#show: codly-init.with()
#codly()

#set heading(numbering: "1.1.")
#set text(lang: "pl")
#set par(justify: true)

#let listing(body, caption: []) = {
  figure(align(left, body), caption: caption, supplement: "Listing")
}

#show heading.where(level: 1): it => [
  #pagebreak(weak: true)
  #it
]

#show raw: set text(size: 7.5pt)
#show figure.where(
  kind: raw
): set block(breakable: true)

#page()[
  #set align(center)
  #set text(font: "Titillium", size: 11pt)

  #v(1em)
  
  #image("logo.png", width: 14.2cm)
  
  #pad(top: 0cm, bottom: 0.21cm, rest: 0pt)[
    #text(weight: "bold")[
      Wydział Informatyki, Elektroniki i Telekomunikacji
    ]
  ]

  #pad(top: 0.42cm, rest: 0pt)[
    #upper()[
      INSTYTUT INFORMATYKI
    ]
  ]

  #v(2em)

  #pad(top: 0.21cm)[
    #text(size: 18pt)[
      Projekt dyplomowy
    ]
  ]

  #pad(top: 0.35cm)[#v(1em)]

  #text(size: 16pt)[
    _Odtwarzacz dźwięku do systemów automatyki inteligentnego domu_ \
    
    _Sound player for smart home automation systems_ \
  ]

  #align(left + bottom, 
    grid(
      columns: (1fr, 2fr),
      rows: (1.1em),
      [Autor:], [Karol Wojciech Kołdras],
      [Kierunek studiów:], [Informatyka],
      [Opiekun pracy:], [dr inż. Ada Brzoza]
    )
  )

  #v(2em)

  #align(center + bottom)[
    Kraków, 2023
  ]
]

#page[]

#{
	show outline.entry.where(
		level: 1
	): it => {
		v(12pt, weak: true)
		strong(it)
	}

	outline(indent: auto)
}



= Cel prac i wizja produktu
Nietrudno jest zauważyć, że w codziennym życiu większości ludzi dźwięk i muzyka odgrywają bardzo istotną rolę. Wykorzystujemy je do nakierowywania uwagi na rzeczy, które znajdują się poza naszym polem widzenia, pomagają nam w utrzymaniu punktualności, a także stanowią bogatą w różnorodność formę rozrywki. Za przykład tego, jak są dla nas ważne, niech posłuży choćby to, w jak wielu urządzeniach elektronicznych znaleźć można głośnik – nawet jeśli to tylko prosty brzęczek. Niestety ta mnogość ma również swoje wady. Każda maszyna bądź gadżet to zwykle swój mały zamknięty ekosystem, z własnym źródłem dźwięku, interfejsem, czy możliwościami. Zazwyczaj nie ma możliwości, aby rozszerzyć ich funkcjonalność, lub aby wchodziły w interakcje pomiędzy sobą. Muzyka grająca ze słuchawek łatwo może zagłuszyć minutnik dzwoniący w innym pomieszczeniu. 

Taka niekompatybilność jest jednak problemem możliwym do rozwiązania. Rosnąca popularność systemów automatyki domowej, zarówno ze strony konsumentów, jak i producentów elektroniki, pokazuje, że możliwe jest zbudowanie ekosystemu wielu zupełnie niezależnych urządzeń, które mogą wymieniać się informacjami i reagować na zdarzenia. Podobnie nic nie stoi na przeszkodzie, aby zbudować analogiczne rozwiązanie dedykowane dla dźwięku. Oczywiście, nie jest to nowy pomysł. Szybki wgląd w internet pozwala znaleźć szeroką ofertę urządzeń, których zasada działania polega dokładnie na tym. Zwykle są to jednak systemy o zamkniętym kodzie źródłowym, często trudne w dostosowaniu do indywidualnych potrzeb użytkownika. W przestrzeni wolnego i otwartego oprogramowania oraz sprzętu skupienie przypada przede wszystkim na rozwiązania w postaci aplikacji. Ma to jednak tę wadę, że jeśli chcemy, by system taki dostępny był przez całą dobę, wymagane jest przeznaczenie na to urządzenia o wystarczającej mocy obliczeniowej. To przekłada się na zwiększony pobór energii, co z kolei skutkuje większymi kosztami. Niemniej rynek elektroniki oferuje sprzęt dedykowany takim zastosowaniom, który zwykle wykorzystuje o wiele mniej zasobów, nie rezygnując przy tym z możliwości.

Celem niniejszej pracy było zaprojektowanie i zbudowanie prototypu odtwarzacza plików dźwiękowych. Istotnym elementem było dostarczenie możliwie szerokiej gamy funkcjonalności, czyli np. odtwarzania wielu formatów plików, czy tworzenie i zarządzanie playlistami. Najistotniejszą wymaganą funkcjonalnością jest jednak możliwość integracji z systemami automatyki domowej, których zadaniem jest udostępnianie sposobu na wchodzenie w interakcję z wieloma urządzeniami znajdującymi się w domu użytkownika, często pochodzącymi od wielu różnych producentów sprzętu. Do porozumiewania się wykorzystują dobrze znane standardy i protokoły, które jednocześnie pozwalają na elastyczne definiowanie wymienianych danych i poleceń.

= Zakres funkcjonalności
Rozdział ten skupia się na teoretycznych wymaganiach budowanego systemu. Istnieje wiele kierunków, w które można poprowadzić jego rozwój, jednak ta praca skupia się na stworzeniu minimalnego produktu spełniającego niezbędne założenia. 

== Kontekst użytkowania produktu
Interakcja z systemem powinna przebiegać na dwa różne sposoby. Po pierwsze, użytkownik musi być w stanie własnoręcznie obsługiwać urządzenie, bez konieczności wchodzenia w interakcję z dodatkowym sprzętem. W tym celu wykorzystać można wyświetlacz oraz przyciski lub ekran dotykowy. Dodatkowo dla ergonomii warto, aby istniała możliwość sterowania bez konieczności fizycznej obecności przy sprzęcie. Można to osiągnąć, udostępniając stronę internetową, która przez wysyłanie zapytań będzie przekazywała polecenia z powrotem do serwera. 

Po drugie, system musi mieć możliwość wymiany informacji z innymi urządzeniami. W tym celu niezbędne jest wykorzystanie protokołów komunikacyjnych powszechnie wykorzystywanych w systemach automatyki domowej. Jednym z nich jest MQTT, który dzięki swojej prostocie i elastyczności oferuje największe możliwości współpracy z istniejącymi rozwiązaniami.

== Wymagania funkcjonalne
Użytkownik ma możliwość:
- wyboru playlisty lub odtwarzania wszystkich utworów,
- pauzowania i wznawiania odtwarzania,
- przeskoczenia do następnego i poprzedniego utworu,
- podejrzenia postępu odtwarzania oraz całkowitej długości utworu,
- podejrzenia tytułu utworu,
- definiowania playlist,
- podglądu plików na karcie SD.

== Wymagania niefunkcjonalne
System powinien być:
- intuicyjny,
- prosty w zarządzaniu i obsłudze,
- wydajny,
- prosty w rozbudowie.

= Wybrane aspekty realizacji
Poniższy rozdział prezentuje techniczną stronę wykonanego projektu. Poza omówieniem wykorzystanego sprzętu oraz konstrukcji oprogramowania zawiera również instrukcję korzystania z urządzenia.

== Wykorzystane rozwiązania technologiczne

=== Platforma sprzętowa
Jako podstawę do wykonania urządzenia wybrano płytkę rozwojową STM32F746-DISCO@disco od STMicroelectornics. Urządzenie to posiada wszystkie potrzebne elementy do zrealizowania projektu w jednym spójnym opakowaniu. Są to m.in. mikrokontroler, pamięć, wyświetlacz i panel dotykowy, kodek audio, układ zasilania oraz złącza wejścia-wyjścia. Dzięki temu, że komponenty te są już odpowiednio dobrane oraz elektrycznie połączone ze sobą sprawia, że podczas realizacji projektu nie trzeba było się obawiać o projektowanie własnej płytki drukowanej, a jednocześnie zmniejsza to też znacząco ilość możliwych błędów, jakie mogą pojawić się podczas implementacji funkcjonalności. 
Dodatkowo producent udostępnia szerokie wsparcie pod względem rozwoju oprogramowania. Począwszy od dedykowanego zintegrowanego środowiska programistycznego, poprzez narzędzia takie jak STM32CubeMX, które potrafi generować kod konfigurujący komponenty, aż po szeroką gamę przykładów, które znacznie ułatwiają dodawanie kolejnych funkcjonalności.

=== Schemat działania systemu
W skład systemu wchodzą komponenty, które są niezależne od siebie, ale współdziałając ze sobą, pozwalają na uzyskanie pełnej funkcjonalności. Bazą jest system operacyjny czasu rzeczywistego – FreeRTOS@rtos. Wykorzystanie systemu operacyjnego było kluczowe w tej aplikacji, gdyż mamy do czynienia z elementami, które nie tylko wchodzą w interakcję z warstwą sprzętową, ale również wzajemnie ze sobą. Opanowanie takiej złożoności wymaga mechanizmów zarządzania współbieżnością oraz abstrakcji ułatwiających korzystanie z niej, np. wątki, semafory czy kolejki. Do tego potrzebne są sposoby na dynamiczne alokowanie i zwalnianie pamięci zgodnie z zapotrzebowaniem poszczególnych komponentów. Za wszystko to odpowiedzialny jest system operacyjny. Należy jednak pamiętać, że urządzenia o silnie ograniczonych zasobach, jakimi są mikrokontrolery, zwykle nie mogą sobie pozwolić na aplikacyjne systemy operacyjne znane z komputerów osobistych, jak Linux czy Windows. W takich sytuacjach dużo lepiej sprawdzają się systemy operacyjne do zastosowań wbudowanych, które mają ograniczoną funkcjonalność, jednak wykorzystują o wiele mniej zasobów, jednocześnie realizując najistotniejsze zadania. Wybór padł na FreeRTOS, ponieważ jest to system, który spełnia wszystkie niezbędne wymagania, a do tego jest sprawdzony w zastosowaniach komercyjnych i posiada bardzo bogaty ekosystem@rtos.

Do uzyskania komunikacji z systemami automatyki domowej, niezbędny jest wybór właściwego interfejsu. Ponieważ wiele systemów wykorzystuje technologie sieciowe, wybór padł na port Ethernet. Do jego obsługi wymagana jest implementacja kilku różnych protokołów sieciowych. W związku z tym, aby przyspieszyć pracę, wykorzystano istniejącą bibliotekę implementującą stos TCP/IP. W tym aspekcie wybrana została biblioteka FreeRTOS+TCP@tcpip, tworzona jako moduł współpracujący z systemem operacyjnym. Jej zalety to szeroka integracja z FreeRTOS oraz wysoki standard poprawności działania – co zostało potwierdzone uzyskaniem certyfikatu SESIP™@sesip. Biblioteka ta implementuje przede wszystkim protokoły warstw 2, 3 oraz 4 modelu ISO/OSI – m.in. ARP, IP czy TCP – ale także oferuje protokoły warstw wyższych, np. DHCP czy DNS. Również implementacja własnych protokołów jest znacznie ułatwiona, dzięki czemu, bazując na przykładach udostępnionych przez twórców tego oprogramowania, zaimplementowany został protokół HTTP, wraz z niezbędnym dla niego serwerem. Zadaniem serwera HTTP jest wysyłanie właściwych zasobów zgodnie z otrzymanymi żądaniami.

Aby nie być ograniczonym przez niewielkie rozmiary wbudowanych pamięci, możliwości urządzenia mogą zostać rozszerzone przy pomocy karty SD. Do jej obsługi wykorzystano bibliotekę FreeRTOS+FAT@fat, która pozwala na interakcję z systemem plików poprzez interfejs programistyczny zbliżony do tego, który jest dostępny przez operacje wejścia i wyjścia w bibliotece standardowej języka C. Ta sama karta pamięci służy również do przechowywania odtwarzanych utworów.

Do obsługi interfejsu użytkownika dostępnego przez ekran dotykowy urządzenia wykorzystano bibliotekę LVGL@lvgl. Jest to darmowa i otwarta biblioteka o bardzo szerokich możliwościach. Zawiera wszystkie niezbędne elementy do budowania interaktywnych aplikacji i pozwala na niezwykle prostą personalizację, a także dzięki temu, że kod źródłowy jest otwarty, dużo łatwiej jest też analizować przebieg wydarzeń w przypadku wystąpienia błędu. Przy jej pomocy zbudowano interfejs odtwarzacza oraz eksploratora plików dostępnych na karcie pamięci. Odtwarzacz ma możliwość odtwarzania utworów w formatach WAV, FLAC i MP3.

Komunikacja z systemami automatyki domowej została zrealizowana poprzez protokół MQTT. Bazuje on na TCP/IP i umożliwia uporządkowaną, bezstratną, dwukierunkową komunikację opartą o wzorzec publikacja/subskrypcja. Klienci łączą się z centralnym serwerem, nazywanym tutaj brokerem MQTT, i subskrybują wskazane przez siebie tematy, zwykle mając jednocześnie możliwość publikowania w nich własnych wiadomości. Wszyscy, którzy subskrybują wybrany temat, otrzymują opublikowane na nim dane. Dzięki swojej lekkości oraz prostocie działania, protokół ten idealnie sprawdza się w systemach wbudowanych. Implementacja klienta MQTT opiera się o bibliotekę coreMQTT@coremqtt. Wybór ten jest podyktowany prostotą w integracji z FreeRTOS oraz FreeRTOS+TCP.

#figure(caption: "Schemat zależności komponentów systemu")[
	#image("disco_logical.drawio.png", width: 14cm)
] <logical>

== Przegląd podsystemów
Poniższy rozdział koncentruje się na komponentach oprogramowania. Każdy podrozdział opisuje pojedynczy moduł, realizujący odrębną funkcjonalność oraz zwykle posiadający własny wątek w systemie operacyjnym. Poza opisem działania zaprezentowane będą też najistotniejsze fragmenty kodu.

=== Moduł odtwarzania <player>
Zadaniem modułu odtwarzania jest konfiguracja sprzętowego kodeka audio, dekodowanie danych do formatu dla niego zrozumiałego oraz zarządzanie stanem i kolejnością odtwarzania.

Punktem wejściowym jest funkcja `AUDIOPLAYER_Init` (@AUDIOPLAYER_Init), która kolejno:

- inicjalizuje kodek,
- rejestruje dekodery dla różnych formatów plików dźwiękowych,
- wskazuje funkcje, które użyte zostaną do wywołań zwrotnych związanych z mechanizmem DMA dla transferu między pamięcią urządzenia a kodekiem,
- definiuje kolejkę `AudioEvent`, dzięki której możliwe jest odbieranie danych z innych wątków,
- uruchamia wątek `Audio_Thread`, który dedykowany jest do realizacji zadań funkcjonalności odtwarzania.

#listing(caption: [Definicja funkcji `AUDIOPLAYER_Init`])[
```c
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_Init(uint8_t volume) {
	/* Try to Init Audio interface in diffrent config in case of failure */
	BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_AUTO, volume, SAI_AUDIO_FREQUENCY_48K);
	BSP_AUDIO_OUT_SetAudioFrameSlot(CODEC_AUDIOFRAME_SLOT_02);

	/* Initialize internal audio structure */
	haudio.out.state = AUDIOPLAYER_STOP;
	haudio.out.mute = MUTE_OFF;
	haudio.out.volume = volume;

	drv = &wav_decoder;
	drvs[DECODER_WAV] = &wav_decoder;
	drvs[DECODER_FLAC] = &flac_decoder;
	drvs[DECODER_MP3] = &mp3_decoder;

	/* Register audio BSP drivers callbacks */
	AUDIO_IF_RegisterCallbacks(
		AUDIO_TransferComplete_CallBack,
		AUDIO_HalfTransfer_CallBack,
		AUDIO_Error_CallBack
	);

	/* Create Audio Queue */
	osMailQDef(AUDIO_Queue, 10, PlayerEvent);
	AudioEvent = osMailCreate(osMailQ(AUDIO_Queue), NULL);

	/* Create Audio task */
	osThreadDef(osAudio_Thread, Audio_Thread, osPriorityRealtime, 0, 5120);
	AudioThreadId = osThreadCreate(osThread(osAudio_Thread), NULL);

	return AUDIOPLAYER_ERROR_NONE;
}
```
] <AUDIOPLAYER_Init>

Funkcjonalność wątku `Audio_Thread` zawiera się w funkcji przedstawionej poniżej.

#listing(caption: [Definicja funkcji `Audio_Thread` wykonywanej w ramach wątku o tej samej nazwie])[
```c
static void Audio_Thread(void const* argument) {
	uint32_t numOfReadBytes;
	osEvent event;
	for (;;) {
		event = osMailGet(AudioEvent, 100);
		if (event.status != osEventMail) {
			continue;
		}

		PlayerEvent* info = event.value.p;
		switch (info->type) {
			case PLAY_CMD:
				_Play(info->data);
				break;
			case STOP_CMD:
				_Stop();
				break;
			case PAUSE_CMD:
				_Pause();
				break;
			case RESUME_CMD:
				_Resume();
				break;
			case SET_POSITION_CMD:
				_SetPosition(info->data);
				break;
			case SELECT_FILE_CMD:
				_SelectFile(info->ptr);
				break;
			default:
				break;
		}

		if (allowReading && haudio.out.state == AUDIOPLAYER_PLAY && (info->type & 0xFE) == 0) {
			uint8_t* dstBuffer = info->type == TRANSFER_COMPLETE
							   ? &haudio.buff[AUDIO_OUT_BUFFER_SIZE / 2]
							   : &haudio.buff[0];

			numOfReadBytes = drv->read_file_fn(dstBuffer, AUDIO_OUT_BUFFER_SIZE / 2, audio_file);

			if (numOfReadBytes == 0) {
				if (ff_feof(audio_file)) {
					haudio.out.state = AUDIOPLAYER_EOF;
				} else {
					haudio.out.state = AUDIOPLAYER_ERROR;
				}
			}
		}
		osMailFree(AudioEvent, info);
	}
}
```
]

Jej zadaniem jest cykliczne sprawdzanie kolejki `AudioEvent` i jeśli od ostatniego sprawdzenia pojawiło się w niej nowe zdarzenie, odpowiednie obsłużenie go. Rozróżniane są następujące rodzaje:
- `PLAY_CMD` -- Rozpocznij odtwarzanie nowego pliku,
- `STOP_CMD` -- Zakończ odtwarzanie, wyczyść bufor z danymi audio,
- `PAUSE_CMD` -- Wstrzymaj odtwarzanie, pozostaw bufor z danymi audio bez zmian,
- `RESUME_CMD` -- Wznów odtwarzanie,
- `SET_POSITION_CMD` -- Ustaw postęp w odtwarzaniu pliku,
- `SELECT_FILE_CMD` -- Wybierz plik i załaduj odpowiedni dekoder,
- `TRANSFER_HALF` -- Transfer przy użyciu DMA wykorzystał 50% pojemności bufora i jego początkowa połowa może zostać zastąpiona nowymi danymi,
- `TRANSFER_COMPLETE` -- Transfer przy użyciu DMA wykorzystał 100% pojemności bufora i jego końcowa połowa może zostać zastąpiona nowymi danymi.

Typowa kolejność działania przy współpracy z innymi wątkami przebiega wg. poniższego schematu. 
Kiedy użytkownik wybierze utwór, do kolejki wysyłane jest zdarzenie z typem ustawionym na `SELECT_FILE_CMD` oraz ścieżką do pliku, który ma zostać odtworzony. Wątek audio, przy współpracy z modułem systemu plików (@fs), sprawdzi, czy wskazany plik istnieje oraz, czy jest w obsługiwanym formacie. Jeśli tak, załaduje właściwy dekoder i będzie oczekiwał na zdarzenie o typie `PLAY_CMD`. 

Kolejno, po otrzymaniu go, wypełni bufor DMA zdekodowanymi danymi i rozpocznie transfer. W miarę postępu w przesyle informacji DMA, wykorzystując odwołania zwrotne, zgłaszać będzie zdarzenia o typie `TRANSFER_HALF` i `TRANSFER_COMPLETE`, odpowiednio po przetworzeniu połowy i całości przeznaczonej przestrzeni. Jako że DMA zostało skonfigurowane w tryb cykliczny, po osiągnięciu końca bufora, wróci na jego początek i będzie kontynuowało swoje działanie. Opisane powyżej ustawienie sprawia, że przestrzeń na dane jest efektywnie podzielona na 2 równe części i kiedy jedna jest aktywnie odczytywana, druga może zostać nadpisana bez obawy o uszkodzenie treści i vice versa. Pozwala to na przeniesienie o wiele większych ilości danych niż rozmiar zarezerwowanej pamięci, co jest niezbędne, kiedy odtwarzane pliki mogą mieć wiele megabajtów rozmiaru. 

W miarę postępu odtwarzania, wątek po otrzymaniu zdarzeń `TRANSFER_HALF` i `TRANSFER_COMPLETE` dekoduje kolejne fragmenty pliku dźwiękowego, wstawiając je do odpowiedniej części bufora. W międzyczasie użytkownik, korzystając z interfejsu graficznego (@gui), może spauzować, wznowić, a także przesunąć progres do dowolnego punktu. 

Kiedy utwór się zakończy lub pojawi się problem w dekodowaniu, zarejestrowane jest zdarzenie `STOP_CMD`, które przerywa działanie i zeruje zarezerwowaną pamięć. Podobnie jest w przypadku, kiedy wybrany zostanie nowy utwór, jednak wtedy wkrótce pojawi się `SELECT_FILE_CMD` i cały proces rozpocznie się od początku.

=== Moduł interfejsu użytkownika <gui>
Graficzny interfejs użytkownika został zbudowany na podstawie LVGL, lekkiego i elastycznego frameworku o szerokich możliwościach. Dostarcza wiele gotowych elementów jak przyciski, suwaki czy pola tekstowe, a także pozwala na budowanie zaawansowanych animacji, wszystko to przy wykorzystaniu stosunkowo niewielkich zasobów sprzętowych. Celem tego modułu jest udostępnienie użytkownikowi reszty funkcjonalności systemu z poziomu wyświetlacza dotykowego wbudowanego w odtwarzacz. 

Moduł rozpoczyna swoje działanie od funkcji `create_audio_gui`, która uzupełnia parametry domyślnego okna, uruchamia moduł odtwarzania (@player), który jest już wymagany, aby zbudować poprawne interakcje oraz buduje interfejs. Następnie tworzone są kolejka zdarzeń `GuiEvent` oraz wątek `LVGLTimer`. Są one zbudowane w taki sposób, aby zdarzenia odbierane przez system mogły być traktowane tak samo, bez względu na ich źródło -- czy jest to sygnał z wyświetlacza, czy wiadomość pochodząca z innego wątku. Pozwala to w prosty sposób na rozszerzanie możliwości urządzenia. Funkcja `LVGLTimer` cyklicznie wywołuję funkcję `lv_timer_handler`, która służy do aktualizowania wewnętrznego stanu frameworku. Następnie sprawdza, czy w kolejce znajdują się jakieś wiadomości i jeśli tak, obsługuje je. Wspomniane funkcje można zobaczyć w poniższym fragmencie kodu (@create_audio_gui).

#listing(caption: [Definicje funkcji `create_audio_gui` i `LVGLTimer`])[
```c
void create_audio_gui(void) {
	lv_obj_set_style_bg_color(lv_scr_act(), lv_color_hex(0x343247), 0);

	AUDIOPLAYER_Init(50);
	ctrl = _audio_gui_main_create(lv_scr_act());

	osMessageQDef(GuiEventQueue, 1, uint16_t);
	GuiEvent = osMessageCreate(osMessageQ(GuiEventQueue), NULL);

	osThreadDef(lvgl_timer, LVGLTimer, osPriorityRealtime, 0, 4096);
	lvgl_timerHandle = osThreadCreate(osThread(lvgl_timer), NULL);
}

void LVGLTimer(void const* argument) {
	/* USER CODE BEGIN LVGLTimer */
	osEvent event;
	/* Infinite loop */
	for (;;) {
		lv_timer_handler();

		event = osMessageGet(GuiEvent, 10);
		if (event.status != osEventMessage) {
			continue;
		}

		switch (event.value.v) {
		case GUI_PLAY:
			audio_gui_play(selected_file);
			break;
		case GUI_RESUME:
			audio_gui_resume();
			break;
		case GUI_PAUSE:
			audio_gui_pause();
			break;
		case GUI_NEXT:
			audio_gui_next(true);
			break;
		case GUI_PREV:
			audio_gui_next(false);
			break;

		default:
			break;
		}


	}
	/* USER CODE END LVGLTimer */
}
```
] <create_audio_gui>

Elementy interfejsu zbudowane są z prostszych komponentów dostarczanych przez bibliotekę. Razem składane są w hierarchię rodzic-dziecko. Na przykład, kod poniżej (@gui_example) prezentuje fragment definicji przycisków sterowania. 

Najpierw tworzony jest kontener `cont`, który zostaje zdefiniowany jako siatka o 7 kolumnach stałych proporcji i 2 wierszach, które dostosowują się do swojej zawartości. Następnie tworzony jest obiekt `play_obj` jako przycisk z grafiką, którego rodzic jest ustawiony na `cont`. Dalej, różne grafiki są przypisywane do różnych stanów -- co innego zostanie pokazane, kiedy będzie przyciśnięty, a co innego, kiedy puszczony. W kolejnym kroku zostaje przyporządkowany do odpowiedniego miejsca w siatce rodzica. Na koniec przypisane zostaje wywołanie zwrotne w przypadku kliknięcia go.

#listing(caption: [Przykład definiowania elementów interfejsu])[
```c
static lv_obj_t* create_ctrl_box(lv_obj_t* parent) {
	/*Create the control box*/
	lv_obj_t* cont = lv_obj_create(parent);
	lv_obj_remove_style_all(cont);
	lv_obj_set_height(cont, LV_SIZE_CONTENT);
	lv_obj_set_style_pad_bottom(cont, 8, 0);

	static const lv_coord_t grid_col[] = {
		LV_GRID_FR(2),
		LV_GRID_FR(3),
		LV_GRID_FR(5),
		LV_GRID_FR(5),
		LV_GRID_FR(5),
		LV_GRID_FR(3),
		LV_GRID_FR(2),
		LV_GRID_TEMPLATE_LAST
	};
	static const lv_coord_t grid_row[] = {LV_GRID_CONTENT, LV_GRID_CONTENT, LV_GRID_TEMPLATE_LAST};
	lv_obj_set_grid_dsc_array(cont, grid_col, grid_row);

	LV_IMG_DECLARE(img_lv_demo_music_btn_play);

	play_obj = lv_imgbtn_create(cont);
	lv_imgbtn_set_src(play_obj, LV_IMGBTN_STATE_RELEASED, NULL, &img_lv_demo_music_btn_play, NULL);
	lv_imgbtn_set_src(
		play_obj,
		LV_IMGBTN_STATE_CHECKED_RELEASED,
		NULL,
		&img_lv_demo_music_btn_pause,
		NULL
	);
	lv_obj_add_flag(play_obj, LV_OBJ_FLAG_CHECKABLE);
	lv_obj_set_grid_cell(play_obj, LV_GRID_ALIGN_CENTER, 3, 1, LV_GRID_ALIGN_CENTER, 0, 1);

	lv_obj_add_event_cb(play_obj, play_event_click_cb, LV_EVENT_CLICKED, NULL);
	lv_obj_add_flag(play_obj, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_set_width(play_obj, img_lv_demo_music_btn_play.header.w);
```
]<gui_example>

Reszta interfejsu zdefiniowana została w sposób analogiczny, więc została pominięta dla zachowania zwięzłości.

=== Moduł komunikacji sieciowej <net>
Płytka STM32F746-DISCO posiada złącze 8P8C połączone z urządzeniem peryferyjnym Ethernet mikrokontrolera. Do komunikacji sieciowej niezbędne jest jeszcze oprogramowanie sterujące konfiguracją i przepływem danych. W tym celu użyta została biblioteka FreeRTOS+TCP. Integruje się ona bardzo dobrze z systemem FreeRTOS, a także radzi sobie wspaniale, kiedy operacje sieciowe działają na różnych wątkach. Przy nakładzie pracy możliwe jest skonfigurowanie urządzenia oraz obsługa DHCP. Wystarczą do tego:
- właściwa konfiguracja w pliku `FreeRTOSIPConfig.h` -- dla przykładu, aby używać DHCP, w pliku tym należy zdefiniować następujące makro `#define ipconfigUSE_DHCP 1`,
- wywołanie funkcji `FreeRTOS_IPInit` -- przyjmuje ona niezbędne stałe parametry:
	- `ucIPAddress` -- domyślny adres IP dla urządzenia, 
	- `ucNetMask` -- domyślna maska sieci, do której urządzenie próbuje się podłączyć, 
	- `ucGatewayAddress` -- domyślny adres bramy sieci, 
	- `ucDNSServerAddress` -- domyślny adres serwera DNS, 
	- `ucMACAddress` -- adres MAC urządzenia.
	Podane parametry zostaną wykorzystane, tylko jeśli niemożliwe będzie uzyskanie ich poprzez DHCP. Wyjątek stanowi tylko adres MAC, który jest używany zawsze.
Wspomniana funkcja, poza konfiguracją sprzętową, inicjuje również własny wątek do obsługi zdarzeń związanych z aktywnością sieciową. 

Dzięki elastyczności FreeRTOS+TCP możliwe jest dodawanie protokołów, które nie wchodzą w skład jej podstawowych funkcjonalności. W projekcie zostało to wykorzystane do dodania prostego serwera HTTP, który udostępnia stronę internetową pozwalającą na zdalne sterowanie odtwarzaniem z innego dowolnego urządzenia w lokalnej sieci wyposażonego w przeglądarkę internetową. Co więcej, do obsługi tej strony wystawione jest proste REST API, również przy użyciu tego samego serwera.

Serwer HTTP obsługiwany jest w osobnym wątku, w funkcji `prvServerWorkTask`, która wykonuje kod wskazany poniżej(@http_srv). Po utworzeniu lokalnych zmiennych, sprawdzane jest, czy karta SD, na której znajdują się zasoby strony WWW, jest podłączona, czy jej inicjalizacja się zakończyła oraz, czy przebiegła pomyślnie. Dalej należy się upewnić, że interfejs użytkownika jest gotowy do działania, gdyż oznacza to, że moduł odtwarzania dźwięku również jest uruchomiony. Kolejnym krokiem jest ustawienie priorytetu wątku. Następnie funkcja czeka na sygnał z wątku sieciowego o tym, że udało się połączyć z lokalną siecią. Dalej, poprzez funkcję `FreeRTOS_CreateTCPServer` tworzony jest serwer, o parametrach wskazanych w zmiennej `xServerConfiguration`. Zawiera ona strukturę typu `xSERVER_CONFIG`, która w polu `eType` przechowuje informację o rodzaju serwera, jaki ma zostać utworzony -- w tym przypadku HTTP. Na koniec, w nieskończonej pętli wykonywana jest funkcja `FreeRTOS_TCPServerWork`, która nasłuchuje przychodzących połączeń TCP i jeśli, otrzyma poprawne dane, wykonuje funkcję dedykowaną danemu rodzajowi serwera.

#listing(caption: [Definicja funkcji `prvServerWorkTask`])[
```c
static void prvServerWorkTask(void* pvParameters) {
	TCPServer_t* pxTCPServer = NULL;
	const TickType_t xInitialBlockTime = pdMS_TO_TICKS(5000UL);
	const TickType_t xSDCardInsertDelay = pdMS_TO_TICKS(1000UL);

	static const struct xSERVER_CONFIG xServerConfiguration[] = {
		/* Server type, 	port number,	backlog, 	root dir. */
		{eSERVER_HTTP, 80, 12, configHTTP_ROOT},
	};

	/* Remove compiler warning about unused parameter. */
	(void)pvParameters;

	while ((pxDisk = FF_SDDiskInit(mainSD_CARD_DISK_NAME)) == NULL) {
		vTaskDelay(xSDCardInsertDelay);
	}

	FreeRTOS_printf(("SD card detected."));

	create_audio_gui();

	/* The priority of this task can be raised now the disk has been
	 initialised. */
	vTaskPrioritySet(NULL, mainTCP_SERVER_TASK_PRIORITY);

	/* Wait until the network is up before creating the servers.  The
	 notification is given from the network event hook. */
	ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

	/* Create the servers defined by the xServerConfiguration array above. */
	pxTCPServer = FreeRTOS_CreateTCPServer(
		xServerConfiguration,
		sizeof(xServerConfiguration) / sizeof(xServerConfiguration[0])
	);
	configASSERT(pxTCPServer);

	for (;;) {
		prvSDCardDetect();

		/* Run the HTTP and/or FTP servers, as configured above. */
		FreeRTOS_TCPServerWork(pxTCPServer, xInitialBlockTime);
	}
}
```
] <http_srv>

Kod wchodzący w skład serwera HTTP jest zbyt obszerny, by zamieszczać go tutaj, ale w uproszczeniu realizowane są 2 funkcjonalności. Jeśli żądanie HTTP zawiera metodę GET, poprzez moduł systemu plików (@fs), na karcie SD wyszukiwany jest zasób, wskazany w URI. Jeśli nie istnieje, odsyłana jest odpowiedź z kodem błędu "404 Nie znaleziono". Jeśli wskazana metoda to POST, realizowane jest jedno ze zdefiniowanych poleceń sterujących urządzeniem. W zależności od URI dostępne są:
- `/control/play` -- Wznawia i zatrzymuje odtwarzanie,
- `/control/prev` -- Włącza poprzedni utwór w kolejce odtwarzania,
- `/control/next` -- Włącza następny utwór w kolejce odtwarzania,
Każdy z nich ma własną funkcję obsługującą przychodzące dane. Jeśli punkt końcowy w URI nie istnieje, odsyłany jest błąd "404 Nie znaleziono". W przypadku otrzymania dowolnej innej metody serwer wysyła wiadomość z kodem "501 Nie zaimplementowano".

=== Moduł systemu plików <fs>
Moduł systemu plików jest wykorzystywany przez pozostałe komponenty systemu. Dostarcza dostęp do karty SD, na której przechowywane są zasoby serwera HTTP, pliki z muzyką czy playlisty. Zbudowany jest na podstawie biblioteki FreeRTOS+FAT, która zapewnia obsługę systemów plików FAT32, FAT16 i FAT12 oraz jest niezwykle prosta w integracji z systemem FreeRTOS. Udostępnia ona interfejs programistyczny podobny do tego, który można znaleźć w bibliotece standardowej języka C. Podobnie jak w przypadku FreeRTOS+TCP (@net), do uruchomienia jej wystarczy:
- właściwa konfiguracja w pliku `FreeRTOSIPConfig.h` oraz
- wywołanie funkcji `FF_SDDiskInit`, która jako jedyny parametr przyjmuje unikatową w skali systemu nazwę dla dysku. W tym przypadku jest ustawiona na `/`, oznaczającą korzeń całego systemu plików.

=== Moduł klienta MQTT <mqtt>
Zaimplementowany klient MQTT opiera się o bibliotekę coreMQTT oraz jeden z dołączonych do niej przykładów -- "coreMQTT Demo (without TLS)"@mqttdemo. Protokół MQTT bazuje na TCP/IP, jednak wspomniana biblioteka nie realizuje tej części komunikacji. W związku z tym, do poprawnego działania, wymagane jest wykorzystanie modułu sieciowego (@net). 

Zadaniem tego modułu jest subskrybowanie wybranych tematów oraz, kiedy pojawią się w nich nowe wiadomości, odpowiednie reagowanie na nie. Funkcjonalność jest realizowana w dedykowanym wątku, który wykonuje funkcję `prvMQTTTask` (@mqtttask). Na początku inicjuje ona zmienne wykorzystywane w dalszych etapach, a zaraz po tym definiowane jest, jakie tematy mają zostać zasubskrybowane. Następnie wątek oczekuje na powiadomienie, że połączenie sieciowe zostało poprawnie rozpoczęte. Kiedy to nastąpi, nawiązywana jest komunikacja z brokerem zdefiniowanym w konfiguracji. Następnie subskrybowane są wymagane tematy. Kolejnym krokiem jest nieskończona pętla, która przetwarza wszystkie dane przychodzące z serwera, tak długo, jak nie pojawi się żaden błąd. W przeciwnym razie połączenie jest zamykane i po odczekaniu kilku sekund nastąpi próba ponowienia. Warto tutaj wspomnieć, że coreMQTT potrafi samo dbać o podtrzymywanie połączenia w przypadku, kiedy przez dłuższy okres broker nie wysyła żadnej komunikacji. Aby zapewnić działanie tej funkcji, wystarczy cyklicznie wywoływać funkcję `prvProcessLoopWithTimeout`.

#listing(caption: [Definicja funkcji `prvMQTTTask`])[
```c
void prvMQTTTask(void *pvParameters)
{
	uint32_t ulTopicCount = 0U;
	NetworkContext_t xNetworkContext = { 0 };
	PlaintextTransportParams_t xPlaintextTransportParams = { 0 };
	MQTTContext_t xMQTTContext = { 0 };
	MQTTStatus_t xMQTTStatus;
	PlaintextTransportStatus_t xNetworkStatus;

	/* Remove compiler warnings about unused parameters. */
	(void) pvParameters;

	/* Set the pParams member of the network context with desired transport. */
	xNetworkContext.pParams = &xPlaintextTransportParams;

	/* Set the entry time of the application. This entry time will be used
	 * to calculate relative time elapsed in the execution of the application,
	 * by the timer utility function that is provided to the MQTT library.
	 */
	ulGlobalEntryTimeMs = prvGetTimeMs();

	/**************************** Initialize. *****************************/

	prvInitializeTopicBuffers();

	/****************************** Connect. ******************************/

	ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

	for (;;)
			{
		/* Attempt to establish a TLS connection with the MQTT broker. This
		 * connects to the MQTT broker specified in mqttconfigMQTT_BROKER_ENDPOINT, using
		 * the port number specified in mqttconfigMQTT_BROKER_PORT (these macros are defined
		 * in file mqtt_config.h). If the connection fails, attempt to re-connect after a timeout.
		 * The timeout value will be exponentially increased until either the maximum timeout value
		 * is reached, or the maximum number of attempts are exhausted. The function returns a failure status
		 * if the TCP connection cannot be established with the broker after a configured number
		 * of attempts. */
		xNetworkStatus = prvConnectToServerWithBackoffRetries(&xNetworkContext);
		if (xNetworkStatus != PLAINTEXT_TRANSPORT_SUCCESS) {
			vTaskDelay(mqttDELAY_BETWEEN_RETRIES_TICKS);
			continue;
		}

		/* Send an MQTT CONNECT packet over the established TLS connection,
		 * and wait for the connection acknowledgment (CONNACK) packet. */
		LogInfo(( "Creating an MQTT connection to %s.\r\n", mqttconfigMQTT_BROKER_ENDPOINT ));
		prvCreateMQTTConnectionWithBroker(&xMQTTContext, &xNetworkContext);

		/**************************** Subscribe. ******************************/

		/* If the server rejected the subscription request, attempt to resubscribe to the
		 * topic. Attempts are made according to the exponential backoff retry strategy
		 * implemented in BackoffAlgorithm. */
		prvMQTTSubscribeWithBackoffRetries(&xMQTTContext);

		/**************************** Publish and Keep-Alive Loop. ******************************/

		/* Send and process keep-alive messages. */
		for (;;)
				{
			xMQTTStatus = prvProcessLoopWithTimeout(&xMQTTContext, mqttPROCESS_LOOP_TIMEOUT_MS);

			/* Something went wrong, so restart the connection from the beginning. */
			if (xMQTTStatus != MQTTSuccess) {
				break;
			}

			vTaskDelay(50);
		}

		/**************************** Disconnect. ******************************/

		/* Send an MQTT DISCONNECT packet over the already-connected TLS over TCP connection.
		 * There is no corresponding response expected from the broker. After sending the
		 * disconnect request, the client must close the network connection. */
		LogInfo(
				( "Disconnecting the MQTT connection with %s.\r\n", mqttconfigMQTT_BROKER_ENDPOINT ));
		xMQTTStatus = MQTT_Disconnect(&xMQTTContext);

		/* Close the network connection.  */
		Plaintext_FreeRTOS_Disconnect(&xNetworkContext);

		/* Reset SUBACK status for each topic filter after completion of the subscription request cycle. */
		for (ulTopicCount = 0; ulTopicCount < mqttTOPIC_COUNT; ulTopicCount++)
				{
			xTopicFilterContext[ulTopicCount].xSubAckStatus = MQTTSubAckFailure;
		}

		vTaskDelay( mqttDELAY_BETWEEN_RETRIES_TICKS);
	}
}
```
] <mqtttask>

W przypadku kiedy broker poinformuje o nowej wiadomości, wykonywane jest odwołanie zwrotne, realizowane w funkcji `prvEventCallback`. Sprawdza ona, do jakiego tematu przypisana jest odebrana wiadomość, a następnie wykonuje odpowiednią funkcję w systemie. Obecnie zdefiniowane reakcje to:
- temat `/control/play` -- Wznawia lub zatrzymuje odtwarzanie aktualnego utworu;
- temat `/control/prev` -- Włącza poprzedni utwór w kolejce odtwarzania;
- temat `/control/next` -- Włącza następny utwór w kolejce odtwarzania.
Treść wiadomości nie gra roli, jedynie fakt pojawienia się jej w odpowiednim temacie. 

#pagebreak()
== Instrukcja użytkowania
=== Diagram urządzenia

#figure(caption: "Schemat urządzenia (widok z góry)")[
	#image("disco-diag.drawio.png", width: 13cm)
] <diagram>

=== Uruchomienie systemu
Do poprawnego działania urządzenia niezbędne są:
- karta microSD sformatowana jako FAT32,
- głośniki z wtyczką Jack 3,5 mm, podłączone do złącza audio,
- zasilacz 5 V, 500 mA, z wtyczką USB Mini-B, podłączony do złącza zasilania

Aby urządzenie mogło komunikować się w sieci lokalnej, należy połączyć je poprzez złącze Ethernet. W sieci musi działać serwer DHCP, aby system otrzymał prawidłową konfigurację.

=== Dodawanie utworów
Kartę microSD należy wpiąć do komputera i przekopiować wybrane utwory do głównego katalogu. Po ponownym podłączeniu jej do urządzenia nowe utwory będą automatycznie dostępne do odtwarzania.

#figure(caption: "Przykładowa zawartość głównego folderu karty SD")[
	#image("folder_view.png")
] <folder_view>

=== Sterowanie odtwarzaniem

==== Ekran dotykowy

#figure(caption: "Ekran sterowania odtwarzaniem")[
	#image("main_screen.png", width: 13cm)
] <main_screen>

Na powyższym ekranie (@main_screen) widać elementy pozwalające na kontrolowanie odtwarzania. Patrząc od góry do dołu i od lewej do prawej są to kolejno:
- tytuł aktualnie odtwarzanego utworu;
- przycisk "Przewiń wstecz" -- naciśnięcie go włącza poprzedni utwór z listy;
- przycisk "Włącz/Pauza" -- uruchamia lub wstrzymuje odtwarzanie;
- przycisk "Przewiń dalej" -- wciśnięcie go przełączna na kolejny utwór z listy;
- pasek postępu odtwarzania -- wizualnie wskazuje, ile czasu pozostało do końca utworu; naciśnięcie go w dowolnym miejscu pozwala na przewijanie do wybranego momentu;
- aktualny czas odtwarzania oraz całkowita długość;
- uchwyt listy odtwarzania;
- adres urządzenia w sieci lokalnej.

Uchwyt listy odtwarzania pozwala "odsunąć" elementy sterowania, aby podejrzeć listę utworów. Aby to zrobić, należy nacisnąć ekran tuż przy dolnej krawędzi i nie puszczając, przesunąć palec do góry. Domyślnie ukaże się przycisk "Playlists" oraz lista wszystkich utworów znajdujących się na karcie SD, którą można przewijać poprzez przeciąganie w dowolnym miejscu. Krótkie naciśnięcie pozycji na liście rozpocznie odtwarzanie wskazanego pliku. 

#figure(caption: "Przykładowa lista utworów")[
	#image("list_screen.png", width: 13cm)
] <list_screen>

Wciśnięcie "Playlists" pokazuje listę zdefiniowanych list odtwarzania. Wybranie jednej z nich wylistuje wskazywane przez nią pliki. Ponownie, wybranie jednego z nich rozpocznie odtwarzanie.

#figure(caption: "Przykładowe menu playlist")[
	#image("playlists.png", width: 13cm)
] <playlists>


#figure(caption: "Przykładowa playlista")[
	#image("playlist0.png", width: 13cm)
] <playlist0>

==== Interfejs webowy
Adres urządzenia w lokalnej sieci jest widoczny po uruchomieniu w prawym dolnym rogu ekranu. Po wpisaniu go w przeglądarce ukaże się prosty interfejs pozwalający na sterowanie odtwarzaniem. Dostępne funkcjonalności to:
- przycisk "Przewiń wstecz" -- naciśnięcie go włącza poprzedni utwór z listy;
- przycisk "Włącz/Pauza" -- uruchamia lub wstrzymuje odtwarzanie;
- przycisk "Przewiń dalej" -- kliknięcie go przełączna na kolejny utwór z listy.

#figure(caption: "Strona WWW dostępna w sieci lokalnej")[
	#image("web_interface.png", width: 13cm)
] <web_interface>

= Wyniki projektu
Projekt miał na celu stworzenie i udokumentowanie prototypu odtwarzacza dźwięku do systemów automatyki inteligentnego domu, co udało się osiągnąć. Zrealizowano też zdecydowaną większość ze zdefiniowanych wymagań jak np. intuicyjny interfejs czy możliwość współpracy z innymi urządzeniami w lokalnej sieci. 

Niestety ze względu na ograniczenia czasowe wiele z funkcjonalności istnieje w bardzo podstawowej formie. Na przykład playlisty i zarządzanie utworami na nich nie może być jeszcze realizowane z poziomu interfejsu na ekranie dotykowym. Również aplikacja webowa stałaby się bardziej użyteczna, gdyby pokazywała informacje o aktualnie wykonywanym utworze. Dodawanie kolejnych, nowych funkcjonalności także będzie utrudnione, ze względu na osiągnięcie ograniczeń sprzętowych platformy rozwojowej, zarówno pod względem mocy obliczeniowej procesora, jak i rozmiaru pamięci.

Mimo swoich ograniczeń zbudowany prototyp może stanowić dobrą podstawę pod dalszy rozwój. Aplikacja webowa mogłaby zostać rozbudowana jeszcze bardziej i mieć możliwość zarządzania treścią karty SD, w tym np. wgrywania plików. Kolejną możliwością jest dodanie funkcji streamowania utworu bezpośrednio do klienta w przeglądarce. W tym przypadku pozwoliłoby to na to, by platforma rozwojowa stała się przede wszystkim serwerem, który byłby w stanie rozsyłać zawartość równolegle do wielu punktów w sieci lokalnej. 

#bibliography("bibliography.yml")

#show outline: set heading(outlined: true)

#outline(
  title: [Spis rysunków],
  target: figure.where(kind: image),
)

#outline(
  title: [Spis listingów],
  target: figure.where(kind: raw),
)