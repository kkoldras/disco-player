/**
 * @file playlist_explorer.h
 *
 */

#ifndef PLAYLIST_EXPLORER_H
#define PLAYLIST_EXPLORER_H

/*********************
 *      INCLUDES
 *********************/
#include "ff_stdio.h"
#include "lvgl.h"
#include <m3u.h>

/*********************
 *      DEFINES
 *********************/

/*Maximum length of path*/
#define PLAYLIST_EXPLORER_PATH_MAX_LEN (250)

/**********************
 *      TYPEDEFS
 **********************/

typedef enum {
	LV_EXPLORER_SORT_NONE,
	LV_EXPLORER_SORT_KIND,
} playlist_explorer_sort_t;

/*Data of canvas*/
typedef struct {
	lv_obj_t obj;
	lv_obj_t* cont;
	lv_obj_t* head_area;
	lv_obj_t* browser_area;
	lv_obj_t* file_table;
	lv_obj_t* path_label;

	const char* sel_fn;
	char current_path[PLAYLIST_EXPLORER_PATH_MAX_LEN];
	playlist_explorer_sort_t sort;

	m3u_state* m3u_state;
} playlist_explorer_t;

extern const lv_obj_class_t playlist_explorer_class;

/***********************
 * GLOBAL VARIABLES
 ***********************/

/**********************
 * GLOBAL PROTOTYPES
 **********************/
lv_obj_t* playlist_explorer_create(lv_obj_t* parent);

/*=====================
 * Setter functions
 *====================*/

/**
 * Set file_explorer sort
 * @param obj   pointer to a file explorer object
 * @param sort  the sort from 'playlist_explorer_sort_t' enum.
 */
void playlist_explorer_set_sort(lv_obj_t* obj, playlist_explorer_sort_t sort);

/*=====================
 * Getter functions
 *====================*/

/**
 * Get file explorer Selected file
 * @param obj   pointer to a file explorer object
 * @return      pointer to the file explorer selected file name
 */
const char* playlist_explorer_get_selected_file_name(const lv_obj_t* obj);

/**
 * Get file explorer cur path
 * @param obj   pointer to a file explorer object
 * @return      pointer to the file explorer cur path
 */
const char* playlist_explorer_get_current_path(const lv_obj_t* obj);

/**
 * Get file explorer head area obj
 * @param obj   pointer to a file explorer object
 * @return      pointer to the file explorer head area obj(lv_obj)
 */
lv_obj_t* playlist_explorer_get_header(lv_obj_t* obj);

/**
 * Get file explorer path obj(label)
 * @param obj   pointer to a file explorer object
 * @return      pointer to the file explorer path obj(lv_label)
 */
lv_obj_t* playlist_explorer_get_path_label(lv_obj_t* obj);

/**
 * Get file explorer file list obj(lv_table)
 * @param obj   pointer to a file explorer object
 * @return      pointer to the file explorer file table obj(lv_table)
 */
lv_obj_t* playlist_explorer_get_file_table(lv_obj_t* obj);

/**
 * Set file_explorer sort
 * @param obj   pointer to a file explorer object
 * @return the current mode from 'playlist_explorer_sort_t'
 */
playlist_explorer_sort_t playlist_explorer_get_sort(const lv_obj_t* obj);

/*=====================
 * Other functions
 *====================*/

/**
 * Open a specified path
 * @param obj   pointer to a file explorer object
 * @param dir   pointer to the path
 */
void playlist_explorer_open_dir(lv_obj_t* obj, const char* dir);

bool is_end_with(const char* str1, const char* str2);

/**********************
 *      MACROS
 **********************/

#endif /*LV_USE_FILE_EXPLORER*/

