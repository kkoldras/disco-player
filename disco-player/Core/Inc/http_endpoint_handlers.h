/*
 * http_endpoint_handlers.h
 */

#ifndef INC_HTTP_ENDPOINT_HANDLERS_H_
#define INC_HTTP_ENDPOINT_HANDLERS_H_

#include "FreeRTOS_IP.h"
#include "FreeRTOS_HTTP_commands.h"
#include "FreeRTOS_TCP_server.h"
#include "FreeRTOS_server_private.h"
#include "FreeRTOS_Sockets.h"

#define ENDPOINT_HANDLER(name) \
	BaseType_t name(const char* pcUrlData, const char* pcBodyData, size_t uxContentLength)

typedef struct {
	char* url;
	BaseType_t (*handler)(const char* pcUrlData, const char* pcBodyData, size_t uxContentLength);
} ENDPOINT;

extern ENDPOINT_HANDLER(HandlePlay);
extern ENDPOINT_HANDLER(HandlePrev);
extern ENDPOINT_HANDLER(HandleNext);

extern ENDPOINT endpoints[];

#define ENDPOINT_COUNT 3

#endif /* INC_HTTP_ENDPOINT_HANDLERS_H_ */
