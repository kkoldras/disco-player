/*
 * audio_player_gui_main.h
 *
 */

#ifndef INC_AUDIO_PLAYER_GUI_H_
#define INC_AUDIO_PLAYER_GUI_H_

/*********************
 *      INCLUDES
 *********************/
#include "lvgl.h"

/*********************
 *      DEFINES
 *********************/
#define LV_DEMO_MUSIC_HANDLE_SIZE 20

/**********************
 *      TYPEDEFS
 **********************/

typedef enum {
	GUI_NONE = 0,
	GUI_PLAY,
	GUI_RESUME,
	GUI_PAUSE,
	GUI_NEXT,
	GUI_PREV,
	GUI_SET_TIME,
	GUI_UNKNOWN,
} GuiEventType;

/**********************
 * GLOBAL PROTOTYPES
 **********************/

void create_audio_gui(void);

lv_obj_t* _audio_gui_main_create(lv_obj_t* parent);
void audio_gui_play(char* file_path);
void audio_gui_resume(void);
void audio_gui_pause(void);
void audio_gui_next(bool next);
void audio_gui_set_title_label(const char *text);
void audio_gui_set_time_label(const char *text);
void audio_gui_set_address(const char* text);

void notify_gui_play();
void notify_gui_next();
void notify_gui_prev();

/**********************
 *      MACROS
 **********************/

#endif /* INC_AUDIO_PLAYER_GUI_H_ */
