/*
 * flac_decoder.h
 *
 *  Created on: Sep 28, 2023
 *      Author: Karol
 */

#ifndef INC_FLAC_DECODER_H_
#define INC_FLAC_DECODER_H_

#include <audio_decoder.h>
#include "FLAC/stream_decoder.h"

extern AudioDecoder flac_decoder;

#endif /* INC_FLAC_DECODER_H_ */
