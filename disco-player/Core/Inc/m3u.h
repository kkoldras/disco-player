/*
 * m3u.h
 */

#ifndef INC_M3U_H_
#define INC_M3U_H_

#include <ff_stdio.h>
#include <stdbool.h>
#include <stdint.h>

#define EXTM3U_HEADER "#EXTM3U"

typedef struct {
	FF_FILE* file;
	bool is_ext;
	int32_t runtime;
	char title[256];
	char filename[256];
	int32_t index;
} m3u_state;

void m3u_open(char* filename, m3u_state* state);
int m3u_next(m3u_state* state);
int m3u_seek(m3u_state* state, int index);
void m3u_close(m3u_state* state);

#endif /* INC_M3U_H_ */
