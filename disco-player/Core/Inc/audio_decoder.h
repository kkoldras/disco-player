/*
 * audio_decoder.h
 *
 *  Created on: Sep 28, 2023
 *      Author: Karol
 */

#include "ff_stdio.h"

#ifndef INC_AUDIO_DECODER_H_
#define INC_AUDIO_DECODER_H_

typedef struct {
	uint32_t SampleRate;
	uint32_t Duration;
	uint32_t SampleSize;
} Audio_InfoTypedef;

typedef struct {
	size_t (*read_file_fn)(void* dstBuffer, size_t size, FF_FILE* file);
	uint8_t (*set_position_fn)(FF_FILE* file, uint8_t percentage);
	uint8_t (*get_file_info_fn)(FF_FILE* file, Audio_InfoTypedef* info);
	void (*init_fn)(FF_FILE* file);
	void (*deinit_fn)(void);
} AudioDecoder;

enum DECODER_TYPES {
	DECODER_WAV,
	DECODER_FLAC,
	DECODER_MP3,
	DECODER_COUNT
};

#endif /* INC_AUDIO_DECODER_H_ */
