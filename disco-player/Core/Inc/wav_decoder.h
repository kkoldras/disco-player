/*
 * wav_decoder.h
 *
 *  Created on: Sep 28, 2023
 *      Author: Karol
 */

#ifndef INC_WAV_DECODER_H_
#define INC_WAV_DECODER_H_

#include <audio_decoder.h>
#include "audio_if.h"

extern AudioDecoder wav_decoder;

#endif /* INC_WAV_DECODER_H_ */
