/*
 * mp3_decoder.h
 *
 *  Created on: Sep 28, 2023
 *      Author: Karol
 */

#ifndef INC_MP3_DECODER_H_
#define INC_MP3_DECODER_H_

#include <audio_decoder.h>
#include "audio_if.h"

extern AudioDecoder mp3_decoder;

#endif /* INC_MP3_DECODER_H_ */
