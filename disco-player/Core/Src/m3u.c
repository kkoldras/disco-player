/*
 * m3u.c
 */

#include "m3u.h"
#include "string.h"
#include "ctype.h"
#include "ff_stdio.h"

static char *line_buffer = NULL;

// Note: This function returns a pointer to a substring of the original string.
// If the given string was allocated dynamically, the caller must not overwrite
// that pointer with the returned value, since the original pointer must be
// deallocated using the same allocator with which it was allocated.  The return
// value must NOT be deallocated using free() etc.
char* trim_whitespace(char *str)
{
	char *end;

	// Trim leading space
	while (isspace((unsigned char )*str))
		str++;

	if (*str == 0)  // All spaces?
		return str;

	// Trim trailing space
	end = str + strlen(str) - 1;
	while (end > str && isspace((unsigned char )*end))
		end--;

	// Write new null terminator character
	end[1] = '\0';

	return str;
}

void m3u_open(char *filename, m3u_state *state) {
	if (state == NULL)
		return;
	
	FF_FILE *file = ff_fopen(filename, "r+");

	state->file = NULL;
	state->is_ext = false;

	state->index = -1;
	state->runtime = -1;

	if (file == NULL) {
		return;
	}

	line_buffer = malloc(256);
	if (line_buffer == NULL) {
		return;
	}

	if (ff_fgets(line_buffer, 255, file) == NULL) {
		return;
	}

	if (strncmp(trim_whitespace(line_buffer), EXTM3U_HEADER, 7) == 0) {
		state->is_ext = true;
	}
	state->file = file;
	ff_rewind(file);
}

int m3u_next(m3u_state* state) {
	if (state == NULL)
		return 0;

	do {
		if (ff_fgets(line_buffer, 255, state->file) == NULL) {
			return 0;
		}

		char *trimmed = trim_whitespace(line_buffer);
		int len = strlen(trimmed);
		if (len == 0)
			continue;

		if (trimmed[0] == '#') {
			if (state->is_ext == false)
				continue;
			else if (strncmp(trimmed, "#EXTINF:", 8) == 0) {
				trimmed += 8;
				char *comma = strchr(trimmed, ',');
				if (comma == NULL)
					continue;
				char *end;
				int runtime = strtol(trimmed, &end, 10);
				if (runtime != 0 || trimmed != end) {
					state->runtime = runtime;
				}

				strcpy(state->title, comma + 1);
			}
		} else {
			strcpy(state->filename, trimmed);
			state->index = ff_ftell(state->file);
			return 1;
		}
	} while (true);

	return 0;
}

int m3u_seek(m3u_state* state, int index) {
	return 0;
}

void m3u_close(m3u_state *state) {
	free(line_buffer);

	ff_fclose(state->file);

}


