/*
 * audio_player_gui.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include <playlist_explorer.h>
#include "audio_player_gui.h"

#include "audio_player_app.h"
#include <stdio.h>

/*********************
 *      DEFINES
 *********************/

#define INTRO_TIME 2000

#define REPEAT_NONE 0
#define REPEAT_ONCE 1
#define REPEAT_ALL 2

#define PLAY_INACTIVE 0x00
#define PLAY_ACTIVE 0x01

/**********************
 *      TYPEDEFS
 **********************/

typedef union {
	uint32_t d32;

	struct {
		uint32_t repeat : 2;
		uint32_t pause : 1;
		uint32_t play : 1;
		uint32_t stop : 1;
		uint32_t mute : 1;
		uint32_t volume : 8;
		uint32_t reserved : 18;
	} b;
} AudioSettingsTypeDef;

/**********************
 *  STATIC PROTOTYPES
 **********************/

static lv_obj_t* create_cont(lv_obj_t* parent);
static void create_wave_images(lv_obj_t* parent);
static lv_obj_t* create_title_box(lv_obj_t* parent);
static lv_obj_t* create_ctrl_box(lv_obj_t* parent);
static lv_obj_t* create_handle(lv_obj_t* parent);
static lv_obj_t* create_network_info(lv_obj_t* parent);

static void play_event_click_cb(lv_event_t* e);
static void prev_click_event_cb(lv_event_t* e);
static void next_click_event_cb(lv_event_t* e);
static void slider_value_event_cb(lv_event_t *e);
static void timer_cb(lv_timer_t* t);
static void stop_start_anim(lv_timer_t* t);

static uint32_t get_file_duration();
static void _PlayFile(char* filename);
static void FILEMGR_GetFileOnly(char* file, char* path);
void LVGLTimer(void const* argument);

/**********************
 *  STATIC VARIABLES
 **********************/
static lv_obj_t* ctrl;

static lv_obj_t* explorer;
static lv_obj_t* main_cont;
static lv_obj_t* title_label;
static lv_obj_t* time_obj;
static lv_obj_t* total_time_obj;
static lv_obj_t* address_label;
static lv_obj_t* slider_obj;

static uint32_t time_act;
static lv_timer_t* sec_counter_timer;
static const lv_font_t* font_small;
static const lv_font_t* font_large;

static bool playing;
static bool start_anim;

static lv_obj_t* play_obj;
static lv_obj_t* next_obj;
static lv_obj_t* prev_obj;

char* tmp_address = NULL;
static AudioSettingsTypeDef PlayerSettings;
static Audio_InfoTypedef AudioInfo;

static char* selected_file = "audio_sample.wav";
static char* currently_playing_file;
static uint32_t simulated_key = LV_KEY_DOWN;
static bool was_dragged = false;

osThreadId lvgl_timerHandle;
osMessageQId GuiEvent = 0;

void notify_gui_play() {
	if (play_obj != NULL) {
		lv_event_send(play_obj, LV_EVENT_CLICKED, NULL);
	}
}

void notify_gui_next() {
	if (next_obj != NULL) {
		lv_event_send(next_obj, LV_EVENT_CLICKED, NULL);
	}
}

void notify_gui_prev() {
	if (prev_obj != NULL) {
		lv_event_send(prev_obj, LV_EVENT_CLICKED, NULL);
	}
}

static void file_selected_cb(lv_event_t* e) {
	lv_event_code_t code = lv_event_get_code(e);
	lv_obj_t* obj = lv_event_get_target(e);

	if (code == LV_EVENT_VALUE_CHANGED) {
//		const char* cur_path = playlist_explorer_get_current_path(obj);
		const char* sel_fn = playlist_explorer_get_selected_file_name(obj);
//		SEGGER_SYSVIEW_PrintfHost("selected file: %s / current path: %s\n", sel_fn, cur_path);
		if (is_end_with(sel_fn, ".wav")
		 || is_end_with(sel_fn, ".WAV")
		 || is_end_with(sel_fn, ".flac")
		 || is_end_with(sel_fn, ".FLAC")
		 || is_end_with(sel_fn, ".mp3")
		 || is_end_with(sel_fn, ".MP3")) {
			selected_file = (char*)sel_fn;
			notify_gui_play();
		} else {
			notify_gui_next();
		}
	}
}

lv_obj_t* _audio_gui_main_create(lv_obj_t* parent) {
	font_small = &lv_font_montserrat_12;
	font_large = &lv_font_montserrat_16;

	explorer = playlist_explorer_create(parent);
	playlist_explorer_open_dir(explorer, "A:");
	lv_obj_add_event_cb(explorer, file_selected_cb, LV_EVENT_VALUE_CHANGED, NULL);

	lv_table_t* file_table = (lv_table_t*)playlist_explorer_get_file_table(explorer);
	file_table->row_act = 0;
	file_table->col_act = 0;

	/*Create the content of the music player*/
	lv_obj_t* cont = create_cont(parent);
	create_wave_images(cont);
	lv_obj_t* title_box = create_title_box(cont);
	lv_obj_t* ctrl_box = create_ctrl_box(cont);
	lv_obj_t* handle_box = create_handle(cont);
	lv_obj_t* network_box = create_network_info(cont);

	/*Arrange the content into a grid*/
	static const lv_coord_t grid_cols[] = {LV_GRID_FR(3), LV_GRID_FR(1), LV_GRID_TEMPLATE_LAST};
	static const lv_coord_t grid_rows[] = {
		LV_DEMO_MUSIC_HANDLE_SIZE, /*Spacing*/
		LV_GRID_FR(1), /*Spacer*/
		LV_GRID_CONTENT, /*Title box*/
		LV_GRID_FR(1), /*Spacer*/
		LV_GRID_CONTENT, /*Icon box*/
		LV_GRID_FR(3), /*Spacer*/
		LV_GRID_CONTENT, /*Control box*/
		LV_GRID_FR(1), /*Spacer*/
		LV_GRID_CONTENT, /*Handle box*/
		LV_GRID_FR(1), /*Spacer*/
		LV_DEMO_MUSIC_HANDLE_SIZE, /*Spacing*/
		LV_GRID_TEMPLATE_LAST
	};

	lv_obj_set_grid_dsc_array(cont, grid_cols, grid_rows);
	lv_obj_set_style_grid_row_align(cont, LV_GRID_ALIGN_SPACE_BETWEEN, 0);
	lv_obj_set_grid_cell(title_box, LV_GRID_ALIGN_STRETCH, 0, 2, LV_GRID_ALIGN_CENTER, 2, 1);
	lv_obj_set_grid_cell(ctrl_box, LV_GRID_ALIGN_STRETCH, 0, 2, LV_GRID_ALIGN_CENTER, 6, 1);
	lv_obj_set_grid_cell(handle_box, LV_GRID_ALIGN_STRETCH, 0, 2, LV_GRID_ALIGN_CENTER, 8, 1);
	lv_obj_set_grid_cell(network_box, LV_GRID_ALIGN_STRETCH, 1, 1, LV_GRID_ALIGN_CENTER, 8, 1);

	sec_counter_timer = lv_timer_create(timer_cb, 1000, NULL);
	lv_timer_pause(sec_counter_timer);

	/*Animate in the content after the intro time*/
	lv_anim_t a;

	start_anim = true;

	lv_timer_t* timer = lv_timer_create(stop_start_anim, INTRO_TIME + 6000, NULL);
	lv_timer_set_repeat_count(timer, 1);

	lv_anim_init(&a);
	lv_anim_set_path_cb(&a, lv_anim_path_bounce);

	lv_obj_fade_in(title_box, 1000, INTRO_TIME + 1000);
	lv_obj_fade_in(ctrl_box, 1000, INTRO_TIME + 1000);
	lv_obj_fade_in(handle_box, 1000, INTRO_TIME + 1000);

	lv_obj_t* title = lv_label_create(cont);
	lv_label_set_text(title, "DISCO\nWeb-enabled Music Player");
	lv_obj_set_style_text_align(title, LV_TEXT_ALIGN_CENTER, 0);
	lv_obj_set_style_text_font(title, font_large, 0);
	lv_obj_set_style_text_line_space(title, 8, 0);
	lv_obj_fade_out(title, 500, INTRO_TIME);
	lv_obj_set_grid_cell(title, LV_GRID_ALIGN_CENTER, 0, 2, LV_GRID_ALIGN_CENTER, 1, 9);

	lv_obj_update_layout(main_cont);

	return main_cont;
}

void audio_gui_next(bool next) {
	lv_obj_t* file_table = playlist_explorer_get_file_table(explorer);
	uint16_t row, col;
	lv_table_get_selected_cell(file_table, &row, &col);
	//	uint16_t row_cnt = lv_table_get_row_cnt(file_table);

	//	if (next && (row + 1 >= row_cnt)) {
	//		((lv_table_t*) file_table)->row_act = -1;
	//	} else if (!next && (row - 1 < 0)) {
	//		((lv_table_t*) file_table)->row_act = row_cnt;
	//	}

	simulated_key = next ? LV_KEY_DOWN : LV_KEY_UP;
	lv_event_send(file_table, LV_EVENT_KEY, &simulated_key);
}

void audio_gui_play(char* file_path) {
	time_act = 0;
	lv_slider_set_value(slider_obj, 0, LV_ANIM_OFF);
	lv_label_set_text(time_obj, "0:00");

	_PlayFile(file_path);
	audio_gui_resume();
}

void audio_gui_resume(void) {
	playing = true;

	lv_timer_resume(sec_counter_timer);
//	lv_slider_set_range(slider_obj, 0, get_file_duration());

	lv_obj_add_state(play_obj, LV_STATE_CHECKED);
	lv_obj_invalidate(play_obj);
}

void audio_gui_pause(void) {
	playing = false;

	lv_timer_pause(sec_counter_timer);
	lv_obj_clear_state(play_obj, LV_STATE_CHECKED);
	lv_obj_invalidate(play_obj);
}

void _handle_fs_error(lv_fs_res_t result) { SEGGER_SYSVIEW_PrintfHost("Error code: %d\n", result); }

void audio_gui_set_title_label(const char *text) {
	lv_label_set_text(title_label, text);
}

void audio_gui_set_time_label(const char *text) {
	lv_label_set_text(time_obj, text);
}

void audio_gui_set_total_time_label(const char *text) {
	lv_label_set_text(total_time_obj, text);
}

void audio_gui_set_address(const char* text) {
	if (address_label == NULL) {
		tmp_address = lv_mem_alloc(sizeof(char) * 16);
		strncpy(tmp_address, text, 16);
	} else {
		lv_label_set_text(address_label, text);
	}
}

void create_audio_gui(void) {
	lv_obj_set_style_bg_color(lv_scr_act(), lv_color_hex(0x343247), 0);

	AUDIOPLAYER_Init(50);
	ctrl = _audio_gui_main_create(lv_scr_act());

	osMessageQDef(GuiEventQueue, 1, uint16_t);
	GuiEvent = osMessageCreate(osMessageQ(GuiEventQueue), NULL);

	osThreadDef(lvgl_timer, LVGLTimer, osPriorityRealtime, 0, 4096);
	lvgl_timerHandle = osThreadCreate(osThread(lvgl_timer), NULL);
}

void LVGLTimer(void const* argument) {
	/* USER CODE BEGIN LVGLTimer */
	osEvent event;
	/* Infinite loop */
	for (;;) {
		lv_timer_handler();
		//osDelay(10);
		event = osMessageGet(GuiEvent, 10);
		if (event.status != osEventMessage) {
			continue;
		}

		switch (event.value.v) {
		case GUI_PLAY:
			audio_gui_play(selected_file);
			break;
		case GUI_RESUME:
			audio_gui_resume();
			break;
		case GUI_PAUSE:
			audio_gui_pause();
			break;
		case GUI_NEXT:
			audio_gui_next(true);
			break;
		case GUI_PREV:
			audio_gui_next(false);
			break;

		default:
			break;
		}


	}
	/* USER CODE END LVGLTimer */
}

/**********************
 *   STATIC FUNCTIONS
 *********************/

static lv_obj_t* create_cont(lv_obj_t* parent) {
	/*A transparent container in which the player section will be scrolled*/
	main_cont = lv_obj_create(parent);
	lv_obj_clear_flag(main_cont, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_clear_flag(main_cont, LV_OBJ_FLAG_SCROLL_ELASTIC);
	lv_obj_remove_style_all(main_cont); /*Make it transparent*/
	lv_obj_set_size(main_cont, lv_pct(100), lv_pct(100));
	lv_obj_set_scroll_snap_y(main_cont, LV_SCROLL_SNAP_CENTER); /*Snap the children to the center*/

	/*Create a container for the player*/
	lv_obj_t* player = lv_obj_create(main_cont);
	lv_obj_set_y(player, -LV_DEMO_MUSIC_HANDLE_SIZE);
	lv_obj_set_size(player, LV_HOR_RES, LV_VER_RES + LV_DEMO_MUSIC_HANDLE_SIZE * 2);
	lv_obj_clear_flag(player, LV_OBJ_FLAG_SNAPABLE);

	lv_obj_set_style_bg_color(player, lv_color_hex(0xffffff), 0);
	lv_obj_set_style_border_width(player, 0, 0);
	lv_obj_set_style_pad_all(player, 0, 0);
	lv_obj_set_scroll_dir(player, LV_DIR_VER);

	/* Transparent placeholders below the player container
	 * It is used only to snap it to center.*/
	lv_obj_t* placeholder1 = lv_obj_create(main_cont);
	lv_obj_remove_style_all(placeholder1);
	lv_obj_clear_flag(placeholder1, LV_OBJ_FLAG_CLICKABLE);
	//    lv_obj_set_style_bg_color(placeholder1, lv_color_hex(0xff0000), 0);
	//    lv_obj_set_style_bg_opa(placeholder1, LV_OPA_50, 0);

	lv_obj_t* placeholder2 = lv_obj_create(main_cont);
	lv_obj_remove_style_all(placeholder2);
	lv_obj_clear_flag(placeholder2, LV_OBJ_FLAG_CLICKABLE);
	//    lv_obj_set_style_bg_color(placeholder2, lv_color_hex(0x00ff00), 0);
	//    lv_obj_set_style_bg_opa(placeholder2, LV_OPA_50, 0);

	lv_obj_set_size(placeholder1, lv_pct(100), LV_VER_RES);
	lv_obj_set_y(placeholder1, 0);

	lv_obj_set_size(placeholder2, lv_pct(100), LV_VER_RES - 2 * LV_DEMO_MUSIC_HANDLE_SIZE);
	lv_obj_set_y(placeholder2, LV_VER_RES + LV_DEMO_MUSIC_HANDLE_SIZE);

	lv_obj_update_layout(main_cont);

	return player;
}

static void create_wave_images(lv_obj_t* parent) {
	LV_IMG_DECLARE(img_lv_demo_music_wave_top);
	LV_IMG_DECLARE(img_lv_demo_music_wave_bottom);
	lv_obj_t* wave_top = lv_img_create(parent);
	lv_img_set_src(wave_top, &img_lv_demo_music_wave_top);
	lv_obj_set_width(wave_top, LV_HOR_RES);
	lv_obj_align(wave_top, LV_ALIGN_TOP_MID, 0, 0);
	lv_obj_add_flag(wave_top, LV_OBJ_FLAG_IGNORE_LAYOUT);

	lv_obj_t* wave_bottom = lv_img_create(parent);
	lv_img_set_src(wave_bottom, &img_lv_demo_music_wave_bottom);
	lv_obj_set_width(wave_bottom, LV_HOR_RES);
	lv_obj_align(wave_bottom, LV_ALIGN_BOTTOM_MID, 0, 0);
	lv_obj_add_flag(wave_bottom, LV_OBJ_FLAG_IGNORE_LAYOUT);

	LV_IMG_DECLARE(img_lv_demo_music_corner_left);
	LV_IMG_DECLARE(img_lv_demo_music_corner_right);
	lv_obj_t* wave_corner = lv_img_create(parent);
	lv_img_set_src(wave_corner, &img_lv_demo_music_corner_left);
	lv_obj_align(wave_corner, LV_ALIGN_BOTTOM_LEFT, 0, 0);

	lv_obj_add_flag(wave_corner, LV_OBJ_FLAG_IGNORE_LAYOUT);

	wave_corner = lv_img_create(parent);
	lv_img_set_src(wave_corner, &img_lv_demo_music_corner_right);
	lv_obj_align(wave_corner, LV_ALIGN_BOTTOM_RIGHT, 0, 0);

	lv_obj_add_flag(wave_corner, LV_OBJ_FLAG_IGNORE_LAYOUT);
}

static lv_obj_t* create_title_box(lv_obj_t* parent) {
	/*Create the titles*/
	lv_obj_t* cont = lv_obj_create(parent);
	lv_obj_remove_style_all(cont);
	lv_obj_set_height(cont, LV_SIZE_CONTENT);
	lv_obj_set_width(cont, lv_pct(90));
	lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);
	lv_obj_set_flex_align(cont, LV_FLEX_ALIGN_START, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);

	title_label = lv_label_create(cont);
	lv_obj_set_style_text_font(title_label, font_large, 0);
	lv_obj_set_style_text_color(title_label, lv_color_hex(0x504d6d), 0);

	lv_label_set_text(title_label, "Select file to play");
	lv_obj_set_height(title_label, LV_SIZE_CONTENT);
	lv_obj_set_width(title_label, lv_pct(90));
	lv_obj_set_style_text_align(title_label, LV_TEXT_ALIGN_CENTER, 0);

	return cont;
}

static lv_obj_t* create_ctrl_box(lv_obj_t* parent) {
	/*Create the control box*/
	lv_obj_t* cont = lv_obj_create(parent);
	lv_obj_remove_style_all(cont);
	lv_obj_set_height(cont, LV_SIZE_CONTENT);
	lv_obj_set_style_pad_bottom(cont, 8, 0);

	static const lv_coord_t grid_col[] = {
		LV_GRID_FR(2),
		LV_GRID_FR(3),
		LV_GRID_FR(5),
		LV_GRID_FR(5),
		LV_GRID_FR(5),
		LV_GRID_FR(3),
		LV_GRID_FR(2),
		LV_GRID_TEMPLATE_LAST
	};
	static const lv_coord_t grid_row[] = {LV_GRID_CONTENT, LV_GRID_CONTENT, LV_GRID_TEMPLATE_LAST};
	lv_obj_set_grid_dsc_array(cont, grid_col, grid_row);

	LV_IMG_DECLARE(img_lv_demo_music_btn_next);
	LV_IMG_DECLARE(img_lv_demo_music_btn_prev);
	LV_IMG_DECLARE(img_lv_demo_music_btn_play);
	LV_IMG_DECLARE(img_lv_demo_music_btn_pause);

	prev_obj = lv_img_create(cont);
	lv_img_set_src(prev_obj, &img_lv_demo_music_btn_prev);
	lv_obj_set_grid_cell(prev_obj, LV_GRID_ALIGN_CENTER, 2, 1, LV_GRID_ALIGN_CENTER, 0, 1);
	lv_obj_add_event_cb(prev_obj, prev_click_event_cb, LV_EVENT_CLICKED, NULL);
	lv_obj_add_flag(prev_obj, LV_OBJ_FLAG_CLICKABLE);

	play_obj = lv_imgbtn_create(cont);
	lv_imgbtn_set_src(play_obj, LV_IMGBTN_STATE_RELEASED, NULL, &img_lv_demo_music_btn_play, NULL);
	lv_imgbtn_set_src(
		play_obj,
		LV_IMGBTN_STATE_CHECKED_RELEASED,
		NULL,
		&img_lv_demo_music_btn_pause,
		NULL
	);
	lv_obj_add_flag(play_obj, LV_OBJ_FLAG_CHECKABLE);
	lv_obj_set_grid_cell(play_obj, LV_GRID_ALIGN_CENTER, 3, 1, LV_GRID_ALIGN_CENTER, 0, 1);

	lv_obj_add_event_cb(play_obj, play_event_click_cb, LV_EVENT_CLICKED, NULL);
	lv_obj_add_flag(play_obj, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_set_width(play_obj, img_lv_demo_music_btn_play.header.w);

	next_obj = lv_img_create(cont);
	lv_img_set_src(next_obj, &img_lv_demo_music_btn_next);
	lv_obj_set_grid_cell(next_obj, LV_GRID_ALIGN_CENTER, 4, 1, LV_GRID_ALIGN_CENTER, 0, 1);
	lv_obj_add_event_cb(next_obj, next_click_event_cb, LV_EVENT_CLICKED, NULL);
	lv_obj_add_flag(next_obj, LV_OBJ_FLAG_CLICKABLE);

	LV_IMG_DECLARE(img_lv_demo_music_slider_knob);
	slider_obj = lv_slider_create(cont);
	lv_slider_set_range(slider_obj, 0, 100);
	lv_obj_set_style_anim_time(slider_obj, 100, 0);
//	lv_obj_add_flag(slider_obj, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_add_event_cb(slider_obj, slider_value_event_cb, LV_EVENT_RELEASED, NULL);
	lv_obj_set_height(slider_obj, 3);
	lv_obj_set_grid_cell(slider_obj, LV_GRID_ALIGN_STRETCH, 1, 4, LV_GRID_ALIGN_CENTER, 1, 1);

	lv_obj_set_style_bg_img_src(slider_obj, &img_lv_demo_music_slider_knob, LV_PART_KNOB);
	lv_obj_set_style_bg_opa(slider_obj, LV_OPA_TRANSP, LV_PART_KNOB);
	lv_obj_set_style_pad_all(slider_obj, 20, LV_PART_KNOB);
	lv_obj_set_style_bg_grad_dir(slider_obj, LV_GRAD_DIR_HOR, LV_PART_INDICATOR);
	lv_obj_set_style_bg_color(slider_obj, lv_color_hex(0x569af8), LV_PART_INDICATOR);
	lv_obj_set_style_bg_grad_color(slider_obj, lv_color_hex(0xa666f1), LV_PART_INDICATOR);
	lv_obj_set_style_outline_width(slider_obj, 0, 0);

	lv_obj_t* time_cont = lv_obj_create(cont);
	lv_obj_remove_style_all(time_cont);
	lv_obj_set_flex_flow(time_cont, LV_FLEX_FLOW_ROW);
	lv_obj_set_flex_align(time_cont, LV_FLEX_ALIGN_START, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);
	lv_obj_set_grid_cell(time_cont, LV_GRID_ALIGN_START, 5, 1, LV_GRID_ALIGN_CENTER, 1, 1);
	lv_obj_set_height(time_cont, LV_SIZE_CONTENT);
	lv_obj_set_style_pad_left(time_cont, 5, 0);

	time_obj = lv_label_create(time_cont);
	lv_obj_set_style_text_font(time_obj, font_small, 0);
	lv_obj_set_style_text_color(time_obj, lv_color_hex(0x8a86b8), 0);
	lv_label_set_text(time_obj, "0:00");

	total_time_obj = lv_label_create(time_cont);
	lv_obj_set_style_text_font(total_time_obj, font_small, 0);
	lv_obj_set_style_text_color(total_time_obj, lv_color_hex(0x8a86b8), 0);
	lv_label_set_text(total_time_obj, "/0:00");

	return cont;
}

static lv_obj_t* create_handle(lv_obj_t* parent) {
	lv_obj_t* cont = lv_obj_create(parent);
	lv_obj_remove_style_all(cont);

	lv_obj_set_size(cont, lv_pct(100), LV_SIZE_CONTENT);
	lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);
	lv_obj_set_flex_align(cont, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);
	lv_obj_set_style_pad_row(cont, 8, 0);

	/*A handle to scroll to the track list*/
	lv_obj_t* handle_label = lv_label_create(cont);
	lv_label_set_text(handle_label, "ALL TRACKS");
	lv_obj_set_style_text_font(handle_label, font_small, 0);
	lv_obj_set_style_text_color(handle_label, lv_color_hex(0x8a86b8), 0);

	lv_obj_t* handle_rect = lv_obj_create(cont);
	lv_obj_set_size(handle_rect, 20, 2);

	lv_obj_set_style_bg_color(handle_rect, lv_color_hex(0x8a86b8), 0);
	lv_obj_set_style_border_width(handle_rect, 0, 0);

	return cont;
}

static lv_obj_t* create_network_info(lv_obj_t* parent) {
	/*Create the titles*/
	lv_obj_t* cont = lv_obj_create(parent);
	lv_obj_remove_style_all(cont);
	lv_obj_set_height(cont, LV_SIZE_CONTENT);
	lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);
	lv_obj_set_flex_align(cont, LV_FLEX_ALIGN_START, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);

	address_label = lv_label_create(cont);
	lv_obj_set_style_text_font(address_label, font_small, 0);
	lv_obj_set_style_text_color(address_label, lv_color_hex(0x504d6d), 0);
	if (tmp_address == NULL || strlen(tmp_address) < 7) {
		lv_label_set_text(address_label, "");
	} else {
		lv_label_set_text(address_label, tmp_address);
		lv_mem_free(tmp_address);
	}

	return cont;
}

static void play_event_click_cb(lv_event_t* e) {
	//	lv_obj_t *obj = lv_event_get_target(e);
	if (strcmp(selected_file, currently_playing_file) != 0) {
		osMessagePut(GuiEvent, GUI_PLAY, 0);
	} else {
		if (haudio.out.state != AUDIOPLAYER_PLAY) {
			osMessagePut(GuiEvent, GUI_RESUME, 0);
			AUDIOPLAYER_Resume();
		} else {
			osMessagePut(GuiEvent, GUI_PAUSE, 0);
			AUDIOPLAYER_Pause();
		}
	}
}

static void prev_click_event_cb(lv_event_t* e) {
	lv_event_code_t code = lv_event_get_code(e);
	if (code == LV_EVENT_CLICKED) {
		osMessagePut(GuiEvent, GUI_PREV, 0);
	}
}

static void next_click_event_cb(lv_event_t* e) {
	lv_event_code_t code = lv_event_get_code(e);
	if (code == LV_EVENT_CLICKED) {
		osMessagePut(GuiEvent, GUI_NEXT, 0);
	}
}

static void slider_value_event_cb(lv_event_t *e) {
	lv_obj_t* slider = lv_event_get_target(e);
	uint32_t position = lv_slider_get_value(slider_obj);

	AUDIOPLAYER_SetPosition(position);
}

static void timer_cb(lv_timer_t* t) {
	LV_UNUSED(t);
	time_act++;
	AUDIOPLAYER_Process();
	lv_label_set_text_fmt(time_obj, "%" LV_PRIu32 ":%02" LV_PRIu32, time_act / 60, time_act % 60);
	lv_slider_set_value(slider_obj, time_act, LV_ANIM_ON);
}

static void stop_start_anim(lv_timer_t* t) {
	LV_UNUSED(t);
	start_anim = false;
	//	lv_obj_refresh_ext_draw_size(spectrum_obj);
}

static uint32_t get_file_duration() {
	if (AudioInfo.Duration) {
		return AudioInfo.Duration;
	}
	return 0;
}

/**
 * @brief  Play wav file.
 * @param  filename: pointer to file name.
 * @retval None
 */
static void _PlayFile(char* filename) {
	uint32_t duration;
	static char tmp[256];
	if (haudio.out.state != AUDIOPLAYER_STOP) {
		AUDIOPLAYER_Stop();
		while (haudio.out.state != AUDIOPLAYER_STOP) {
			osDelay(1);
		}
	}

	if (haudio.out.state == AUDIOPLAYER_STOP) {
		if (AUDIOPLAYER_GetFileInfo(filename, &AudioInfo) == 0) {
			/* Title */
			FILEMGR_GetFileOnly(tmp, filename);
			audio_gui_set_title_label(tmp);

			/* Total Time */
			duration = get_file_duration();
			sprintf((char*)tmp, "/%02lu:%02lu", duration / 60, duration % 60);
			audio_gui_set_total_time_label(tmp);

			/* Open audio file */
			if (AUDIOPLAYER_SelectFile(filename) == 0) {
				/* start playing */
				currently_playing_file = filename;
				AUDIOPLAYER_Play(AudioInfo.SampleRate);
				if (PlayerSettings.b.mute == MUTE_ON) {
					AUDIOPLAYER_Mute(MUTE_ON);
				}
			}
		} else {
			audio_gui_set_title_label("Unknown");
			audio_gui_set_total_time_label("/0:00");
		}
	}
}

/**
 * @brief  Notify the end of wav file.
 * @param  None.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_NotifyEndOfFile(void) {
	AUDIOPLAYER_Stop();
	osMessagePut(GuiEvent, GUI_NEXT, 0);

	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Retrieve the file name from a full file path
 * @param  file: pointer to base path
 * @param  path: pointer to full path
 * @retval None
 */
static void FILEMGR_GetFileOnly(char* file, char* path) {
	char *baseName1, *baseName2;
	baseName1 = strrchr(path, '/');
	baseName2 = strrchr(path, ':');

	if (baseName1++) {
		strcpy(file, baseName1);
	} else {
		if (baseName2++) {
			strcpy(file, baseName2);
		} else {
			strcpy(file, path);
		}
	}
}
