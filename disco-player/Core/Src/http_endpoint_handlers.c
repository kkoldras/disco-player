/*
 * http_endpoint_handlers.c
 */

#include "http_endpoint_handlers.h"

#include "audio_player_gui.h"

ENDPOINT_HANDLER(HandlePlay) {
	BaseType_t xCode = WEB_NO_CONTENT;
	notify_gui_play();
	return xCode;
}

ENDPOINT_HANDLER(HandlePrev) {
	BaseType_t xCode = WEB_NO_CONTENT;
	notify_gui_prev();
	return xCode;
}

ENDPOINT_HANDLER(HandleNext) {
	BaseType_t xCode = WEB_NO_CONTENT;
	notify_gui_next();
	return xCode;
}

ENDPOINT endpoints[] = {
	{.url = "/control/play", .handler = HandlePlay},
	{.url = "/control/prev", .handler = HandlePrev},
	{.url = "/control/next", .handler = HandleNext},
};
