/*
 * wav_decoder.c
 *
 *  Created on: Sep 28, 2023
 *      Author: Karol
 */

#include <wav_decoder.h>
#include "SEGGER_SYSVIEW.h"

size_t wav_read_file(void* dstBuffer, size_t size, FF_FILE* file) {
	return ff_fread(dstBuffer, 1, size, file);
}

uint8_t wav_set_position(FF_FILE* file, uint8_t percentage) {
	long file_pos;
	size_t file_len = ff_filelength(file);

	file_pos = file_len / AUDIO_OUT_BUFFER_SIZE / 100;
	file_pos *= (percentage * AUDIO_OUT_BUFFER_SIZE);
	file_pos = (file_len <= file_pos) ? file_len : file_pos;
	if (ff_fseek(file, file_pos, FF_SEEK_SET) == -1) {
		return 0;
	}

	return 1;
}

uint8_t wav_get_file_info(FF_FILE* file, Audio_InfoTypedef* info) {
	uint32_t numOfReadBytes;
	WAV_InfoTypedef wav_info;

	long tmpPos = ff_ftell(file);
	if (ff_fseek(file, 0, FF_SEEK_SET) != 0) {
		return 0;
	}

	numOfReadBytes = ff_fread(&wav_info, 1, sizeof(WAV_InfoTypedef), file);

	if (numOfReadBytes == sizeof(WAV_InfoTypedef)) {
		if ((wav_info.ChunkID == 0x46464952) && (wav_info.AudioFormat == 1)) {
			if (info) {
				info->Duration = wav_info.FileSize / wav_info.ByteRate;
				info->SampleRate = wav_info.SampleRate;
				info->SampleSize = wav_info.BitPerSample;
			}
			if (ff_fseek(file, tmpPos, FF_SEEK_SET) != 0) {
				return 0;
			}
			return 1;
		}
	} else {
		SEGGER_SYSVIEW_PrintfHost("Can't read file. Code %d", stdioGET_ERRNO());
	}

	ff_fseek(file, tmpPos, FF_SEEK_SET);
	return 0;
}

void wav_init(FF_FILE* file) { ff_fseek(file, sizeof(WAV_InfoTypedef), FF_SEEK_SET); }
static void wav_deinit(void) { }

AudioDecoder wav_decoder = {
	.get_file_info_fn = wav_get_file_info,
	.read_file_fn = wav_read_file,
	.set_position_fn = wav_set_position,
	.init_fn = wav_init,
	.deinit_fn = wav_deinit
};
