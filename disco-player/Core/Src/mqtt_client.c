#include "mqtt_client.h"
#include "http_endpoint_handlers.h"

/**
 * @brief Connect to MQTT broker with reconnection retries.
 *
 * If connection fails, retry is attempted after a timeout.
 * Timeout value will exponentially increase until the maximum
 * timeout value is reached or the number of attempts are exhausted.
 *
 * @param[out] pxNetworkContext The output parameter to return the created network context.
 *
 * @return The status of the final connection attempt.
 */
static PlaintextTransportStatus_t prvConnectToServerWithBackoffRetries( NetworkContext_t * pxNetworkContext );

/**
 * @brief Sends an MQTT Connect packet over the already connected TLS over TCP connection.
 *
 * @param[in, out] pxMQTTContext MQTT context pointer.
 * @param[in] xNetworkContext network context.
 */
static void prvCreateMQTTConnectionWithBroker( MQTTContext_t * pxMQTTContext,
                                               NetworkContext_t * pxNetworkContext );

/**
 * @brief Function to update variable #Context with status
 * information from Subscribe ACK. Called by the event callback after processing
 * an incoming SUBACK packet.
 *
 * @param[in] Server response to the subscription request.
 */
static void prvUpdateSubAckStatus( MQTTPacketInfo_t * pxPacketInfo );

/**
 * @brief Subscribes to the topic as specified in mqttTOPIC at the top of
 * this file. In the case of a Subscribe ACK failure, then subscription is
 * retried using an exponential backoff strategy with jitter.
 *
 * @param[in] pxMQTTContext MQTT context pointer.
 */
static void prvMQTTSubscribeWithBackoffRetries( MQTTContext_t * pxMQTTContext );

/**
 * @brief The timer query function provided to the MQTT context.
 *
 * @return Time in milliseconds.
 */
static uint32_t prvGetTimeMs( void );

/**
 * @brief Process a response or ack to an MQTT request (PING, PUBLISH,
 * SUBSCRIBE or UNSUBSCRIBE). This function processes PINGRESP, PUBACK,
 * PUBREC, PUBREL, PUBCOMP, SUBACK, and UNSUBACK.
 *
 * @param[in] pxIncomingPacket is a pointer to structure containing deserialized
 * MQTT response.
 * @param[in] usPacketId is the packet identifier from the ack received.
 */
static void prvMQTTProcessResponse( MQTTPacketInfo_t * pxIncomingPacket,
                                    uint16_t usPacketId );

/**
 * @brief Process incoming Publish message.
 *
 * @param[in] pxPublishInfo is a pointer to structure containing deserialized
 * Publish message.
 */
static void prvMQTTProcessIncomingPublish( MQTTPublishInfo_t * pxPublishInfo );

/**
 * @brief The application callback function for getting the incoming publishes,
 * incoming acks, and ping responses reported from the MQTT library.
 *
 * @param[in] pxMQTTContext MQTT context pointer.
 * @param[in] pxPacketInfo Packet Info pointer for the incoming packet.
 * @param[in] pxDeserializedInfo Deserialized information from the incoming packet.
 */
static void prvEventCallback( MQTTContext_t * pxMQTTContext,
                              MQTTPacketInfo_t * pxPacketInfo,
                              MQTTDeserializedInfo_t * pxDeserializedInfo );

/**
 * @brief Call #MQTT_ProcessLoop in a loop for the duration of a timeout or
 * #MQTT_ProcessLoop returns a failure.
 *
 * @param[in] pMqttContext MQTT context pointer.
 * @param[in] ulTimeoutMs Duration to call #MQTT_ProcessLoop for.
 *
 * @return Returns the return value of the last call to #MQTT_ProcessLoop.
 */
static MQTTStatus_t prvProcessLoopWithTimeout( MQTTContext_t * pMqttContext,
                                               uint32_t ulTimeoutMs );

/*-----------------------------------------------------------*/

/**
 * @brief Static buffer used to hold MQTT messages being sent and received.
 */
static uint8_t ucSharedBuffer[ mqttconfigNETWORK_BUFFER_SIZE ];

/**
 * @brief Global entry time into the application to use as a reference timestamp
 * in the #prvGetTimeMs function. #prvGetTimeMs will always return the difference
 * between the current time and the global entry time. This will reduce the chances
 * of overflow for the 32 bit unsigned integer used for holding the timestamp.
 */
static uint32_t ulGlobalEntryTimeMs;

/**
 * @brief Packet Identifier generated when Subscribe request was sent to the broker;
 * it is used to match received Subscribe ACK to the transmitted Subscribe packet.
 */
static uint16_t usSubscribePacketIdentifier;

/**
 * @brief A pair containing a topic filter and its SUBACK status.
 */
typedef struct topicFilterContext
{
	uint8_t pcTopicFilter[mqttTOPIC_BUFFER_SIZE];
    MQTTSubAckStatus_t xSubAckStatus;
} topicFilterContext_t;

/**
 * @brief An array containing the context of a SUBACK; the SUBACK status
 * of a filter is updated when the event callback processes a SUBACK.
 */
static topicFilterContext_t xTopicFilterContext[mqttTOPIC_COUNT];


/** @brief Static buffer used to hold MQTT messages being sent and received. */
static MQTTFixedBuffer_t xBuffer =
{
    ucSharedBuffer,
    mqttconfigNETWORK_BUFFER_SIZE
};

/**
 * @brief Array to track the outgoing publish records for outgoing publishes
 * with QoS > 0.
 *
 * This is passed into #MQTT_InitStatefulQoS to allow for QoS > 0.
 *
 */
static MQTTPubAckInfo_t pOutgoingPublishRecords[mqttOUTGOING_PUBLISH_RECORD_LEN];

/**
 * @brief Array to track the incoming publish records for incoming publishes
 * with QoS > 0.
 *
 * This is passed into #MQTT_InitStatefulQoS to allow for QoS > 0.
 *
 */
static MQTTPubAckInfo_t pIncomingPublishRecords[mqttINCOMING_PUBLISH_RECORD_LEN];

/*-----------------------------------------------------------*/

static void prvInitializeTopicBuffers(void)
{
	uint32_t ulTopicCount;

	/* Write topic strings into buffers. */
	snprintf(xTopicFilterContext[0].pcTopicFilter, mqttTOPIC_BUFFER_SIZE, "/control/#");


	for (ulTopicCount = 0; ulTopicCount < mqttTOPIC_COUNT; ulTopicCount++)
			{
		/* Assign topic string to its corresponding SUBACK code initialized as a failure. */
		xTopicFilterContext[ulTopicCount].xSubAckStatus = MQTTSubAckFailure;
	}
}

/*-----------------------------------------------------------*/

void prvMQTTTask(void *pvParameters)
{
	uint32_t ulTopicCount = 0U;
	NetworkContext_t xNetworkContext = { 0 };
	PlaintextTransportParams_t xPlaintextTransportParams = { 0 };
	MQTTContext_t xMQTTContext = { 0 };
	MQTTStatus_t xMQTTStatus;
	PlaintextTransportStatus_t xNetworkStatus;

	/* Remove compiler warnings about unused parameters. */
	(void) pvParameters;

	/* Set the pParams member of the network context with desired transport. */
	xNetworkContext.pParams = &xPlaintextTransportParams;

	/* Set the entry time of the application. This entry time will be used
	 * to calculate relative time elapsed in the execution of the application,
	 * by the timer utility function that is provided to the MQTT library.
	 */
	ulGlobalEntryTimeMs = prvGetTimeMs();

	/**************************** Initialize. *****************************/

	prvInitializeTopicBuffers();

	/****************************** Connect. ******************************/

	ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

	for (;;)
			{
		/* Attempt to establish a TLS connection with the MQTT broker. This
		 * connects to the MQTT broker specified in mqttconfigMQTT_BROKER_ENDPOINT, using
		 * the port number specified in mqttconfigMQTT_BROKER_PORT (these macros are defined
		 * in file mqtt_config.h). If the connection fails, attempt to re-connect after a timeout.
		 * The timeout value will be exponentially increased until either the maximum timeout value
		 * is reached, or the maximum number of attempts are exhausted. The function returns a failure status
		 * if the TCP connection cannot be established with the broker after a configured number
		 * of attempts. */
		xNetworkStatus = prvConnectToServerWithBackoffRetries(&xNetworkContext);
		if (xNetworkStatus != PLAINTEXT_TRANSPORT_SUCCESS) {
			vTaskDelay(mqttDELAY_BETWEEN_RETRIES_TICKS);
			continue;
		}

		/* Send an MQTT CONNECT packet over the established TLS connection,
		 * and wait for the connection acknowledgment (CONNACK) packet. */
		LogInfo(( "Creating an MQTT connection to %s.\r\n", mqttconfigMQTT_BROKER_ENDPOINT ));
		prvCreateMQTTConnectionWithBroker(&xMQTTContext, &xNetworkContext);

		/**************************** Subscribe. ******************************/

		/* If the server rejected the subscription request, attempt to resubscribe to the
		 * topic. Attempts are made according to the exponential backoff retry strategy
		 * implemented in BackoffAlgorithm. */
		prvMQTTSubscribeWithBackoffRetries(&xMQTTContext);

		/**************************** Publish and Keep-Alive Loop. ******************************/

		/* Send and process keep-alive messages. */
		for (;;)
				{
			xMQTTStatus = prvProcessLoopWithTimeout(&xMQTTContext, mqttPROCESS_LOOP_TIMEOUT_MS);

			/* Something went wrong, so restart the connection from the beginning. */
			if (xMQTTStatus != MQTTSuccess) {
				break;
			}

			vTaskDelay(50);
		}

		/**************************** Disconnect. ******************************/

		/* Send an MQTT DISCONNECT packet over the already-connected TLS over TCP connection.
		 * There is no corresponding response expected from the broker. After sending the
		 * disconnect request, the client must close the network connection. */
		LogInfo(
				( "Disconnecting the MQTT connection with %s.\r\n", mqttconfigMQTT_BROKER_ENDPOINT ));
		xMQTTStatus = MQTT_Disconnect(&xMQTTContext);

		/* Close the network connection.  */
		Plaintext_FreeRTOS_Disconnect(&xNetworkContext);

		/* Reset SUBACK status for each topic filter after completion of the subscription request cycle. */
		for (ulTopicCount = 0; ulTopicCount < mqttTOPIC_COUNT; ulTopicCount++)
				{
			xTopicFilterContext[ulTopicCount].xSubAckStatus = MQTTSubAckFailure;
		}

		vTaskDelay( mqttDELAY_BETWEEN_RETRIES_TICKS);
	}
}
/*-----------------------------------------------------------*/

extern UBaseType_t uxRand(void);

static PlaintextTransportStatus_t prvConnectToServerWithBackoffRetries( NetworkContext_t * pxNetworkContext )
{
    PlaintextTransportStatus_t xNetworkStatus;
    BackoffAlgorithmStatus_t xBackoffAlgStatus = BackoffAlgorithmSuccess;
    BackoffAlgorithmContext_t xReconnectParams;
    uint16_t usNextRetryBackOff = 0U;

    /* Initialize reconnect attempts and interval.*/
    BackoffAlgorithm_InitializeParams( &xReconnectParams,
	mqttRETRY_BACKOFF_BASE_MS,
	mqttRETRY_MAX_BACKOFF_DELAY_MS,
	mqttRETRY_MAX_ATTEMPTS);

    /* Attempt to connect to MQTT broker. If connection fails, retry after
     * a timeout. Timeout value will exponentially increase till maximum
     * attempts are reached.
     */
    do
    {
		/* Establish a TCP connection with the MQTT broker. This connects to
		 * the MQTT broker as specified in mqttconfigMQTT_BROKER_ENDPOINT and
		 * mqttconfigMQTT_BROKER_PORT at the top of this file. */
        LogInfo( ( "Create a TCP connection to %s:%d.",
                   mqttconfigMQTT_BROKER_ENDPOINT,
                   mqttconfigMQTT_BROKER_PORT ) );
        xNetworkStatus = Plaintext_FreeRTOS_Connect( pxNetworkContext,
                                                     mqttconfigMQTT_BROKER_ENDPOINT,
                                                     mqttconfigMQTT_BROKER_PORT,
		mqttTRANSPORT_SEND_RECV_TIMEOUT_MS,
		mqttTRANSPORT_SEND_RECV_TIMEOUT_MS);

        if( xNetworkStatus != PLAINTEXT_TRANSPORT_SUCCESS )
        {
            /* Generate a random number and calculate backoff value (in milliseconds) for
             * the next connection retry.
             * Note: It is recommended to seed the random number generator with a device-specific
             * entropy source so that possibility of multiple devices retrying failed network operations
             * at similar intervals can be avoided. */
            xBackoffAlgStatus = BackoffAlgorithm_GetNextBackoff( &xReconnectParams, uxRand(), &usNextRetryBackOff );

            if( xBackoffAlgStatus == BackoffAlgorithmRetriesExhausted )
            {
                LogError( ( "Connection to the broker failed, all attempts exhausted." ) );
            }
            else if( xBackoffAlgStatus == BackoffAlgorithmSuccess )
            {
                LogWarn( ( "Connection to the broker failed. "
                           "Retrying connection with backoff and jitter." ) );
                vTaskDelay( pdMS_TO_TICKS( usNextRetryBackOff ) );
            }
        }
    } while( ( xNetworkStatus != PLAINTEXT_TRANSPORT_SUCCESS ) && ( xBackoffAlgStatus == BackoffAlgorithmSuccess ) );

    return xNetworkStatus;
}
/*-----------------------------------------------------------*/

static void prvCreateMQTTConnectionWithBroker( MQTTContext_t * pxMQTTContext,
                                               NetworkContext_t * pxNetworkContext )
{
    MQTTStatus_t xResult;
    MQTTConnectInfo_t xConnectInfo;
    bool xSessionPresent;
    TransportInterface_t xTransport;

    /***
     * For readability, error handling in this function is restricted to the use of
     * asserts().
     ***/

    /* Fill in Transport Interface send and receive function pointers. */
    xTransport.pNetworkContext = pxNetworkContext;
    xTransport.send = Plaintext_FreeRTOS_send;
    xTransport.recv = Plaintext_FreeRTOS_recv;
    xTransport.writev = NULL;

    /* Initialize MQTT library. */
    xResult = MQTT_Init( pxMQTTContext, &xTransport, prvGetTimeMs, prvEventCallback, &xBuffer );
    configASSERT( xResult == MQTTSuccess );
    xResult = MQTT_InitStatefulQoS( pxMQTTContext,
                                    pOutgoingPublishRecords,
			mqttOUTGOING_PUBLISH_RECORD_LEN,
                                    pIncomingPublishRecords,
			mqttINCOMING_PUBLISH_RECORD_LEN);
    configASSERT( xResult == MQTTSuccess );

	/* Some fields are not used so start with everything at 0. */
    ( void ) memset( ( void * ) &xConnectInfo, 0x00, sizeof( xConnectInfo ) );

    /* Start with a clean session i.e. direct the MQTT broker to discard any
     * previous session data. Also, establishing a connection with clean session
     * will ensure that the broker does not store any data when this client
     * gets disconnected. */
    xConnectInfo.cleanSession = true;

    /* The client identifier is used to uniquely identify this MQTT client to
     * the MQTT broker. In a production device the identifier can be something
     * unique, such as a device serial number. */
    xConnectInfo.pClientIdentifier = mqttconfigCLIENT_IDENTIFIER;
    xConnectInfo.clientIdentifierLength = ( uint16_t ) strlen( mqttconfigCLIENT_IDENTIFIER );

    /* Set MQTT keep-alive period. If the application does not send packets at an interval less than
     * the keep-alive period, the MQTT library will send PINGREQ packets. */
	xConnectInfo.keepAliveSeconds = mqttKEEP_ALIVE_TIMEOUT_SECONDS;

	/* Send MQTT CONNECT packet to broker. LWT is not used, so it is passed as NULL. */
    xResult = MQTT_Connect( pxMQTTContext,
                            &xConnectInfo,
                            NULL,
			mqttCONNACK_RECV_TIMEOUT_MS,
                            &xSessionPresent );
	if (xResult != MQTTSuccess) {
		return;
	}

    /* Successfully established and MQTT connection with the broker. */
    LogInfo( ( "An MQTT connection is established with %s.", mqttconfigMQTT_BROKER_ENDPOINT ) );
}
/*-----------------------------------------------------------*/

static void prvUpdateSubAckStatus( MQTTPacketInfo_t * pxPacketInfo )
{
    MQTTStatus_t xResult = MQTTSuccess;
    uint8_t * pucPayload = NULL;
    size_t ulSize = 0;
    uint32_t ulTopicCount = 0U;

    xResult = MQTT_GetSubAckStatusCodes( pxPacketInfo, &pucPayload, &ulSize );

    /* MQTT_GetSubAckStatusCodes always returns success if called with packet info
     * from the event callback and non-NULL parameters. */
	if (xResult != MQTTSuccess) {
		return;
	}

	for (ulTopicCount = 0; ulTopicCount < ulSize; ulTopicCount++)
			{
		xTopicFilterContext[ulTopicCount].xSubAckStatus = pucPayload[ulTopicCount];
	}
}
/*-----------------------------------------------------------*/

static void prvMQTTSubscribeWithBackoffRetries( MQTTContext_t * pxMQTTContext )
{
    MQTTStatus_t xResult = MQTTSuccess;
    BackoffAlgorithmStatus_t xBackoffAlgStatus = BackoffAlgorithmSuccess;
    BackoffAlgorithmContext_t xRetryParams;
    uint16_t usNextRetryBackOff = 0U;
	MQTTSubscribeInfo_t xMQTTSubscription[mqttTOPIC_COUNT];
    bool xFailedSubscribeToTopic = false;
    uint32_t ulTopicCount = 0U;

	/* Some fields not used so start with everything at 0. */
	(void) memset((void*) &xMQTTSubscription, 0x00, sizeof(xMQTTSubscription));

    /* Get a unique packet id. */
    usSubscribePacketIdentifier = MQTT_GetPacketId( pxMQTTContext );

	/* Populate subscription list. */
	for (ulTopicCount = 0; ulTopicCount < mqttTOPIC_COUNT; ulTopicCount++)
			{
		xMQTTSubscription[ulTopicCount].qos = MQTTQoS2;
		xMQTTSubscription[ulTopicCount].pTopicFilter =
				xTopicFilterContext[ulTopicCount].pcTopicFilter;
		xMQTTSubscription[ulTopicCount].topicFilterLength = (uint16_t) strlen(
				xTopicFilterContext[ulTopicCount].pcTopicFilter);
	}

    /* Initialize context for backoff retry attempts if SUBSCRIBE request fails. */
    BackoffAlgorithm_InitializeParams( &xRetryParams,
	mqttRETRY_BACKOFF_BASE_MS,
	mqttRETRY_MAX_BACKOFF_DELAY_MS,
	mqttRETRY_MAX_ATTEMPTS);

    do
    {
		/* The client is now connected to the broker. Subscribe to requested topics
		 * by sending a subscribe packet then waiting for a subscribe acknowledgment
		 * (SUBACK).*/
        xResult = MQTT_Subscribe( pxMQTTContext,
                                  xMQTTSubscription,
                                  sizeof( xMQTTSubscription ) / sizeof( MQTTSubscribeInfo_t ),
                                  usSubscribePacketIdentifier );
		if (xResult != MQTTSuccess) {
			return;
		}

		for (ulTopicCount = 0; ulTopicCount < mqttTOPIC_COUNT; ulTopicCount++)
        {
            LogInfo( ( "SUBSCRIBE sent for topic %s to broker.\n\n",
                       xTopicFilterContext[ ulTopicCount ].pcTopicFilter ) );
        }

        /* Process incoming packet from the broker. After sending the subscribe, the
         * client may receive a publish before it receives a subscribe ack. Therefore,
		 * call generic incoming packet processing function. */
		xResult = prvProcessLoopWithTimeout(pxMQTTContext, mqttPROCESS_LOOP_TIMEOUT_MS);
		if (xResult != MQTTSuccess) {
			return;
		}

        /* Reset flag before checking suback responses. */
        xFailedSubscribeToTopic = false;

        /* Check if recent subscription request has been rejected. #xTopicFilterContext is updated
         * in the event callback to reflect the status of the SUBACK sent by the broker. It represents
         * either the QoS level granted by the server upon subscription, or acknowledgement of
         * server rejection of the subscription request. */
		for (ulTopicCount = 0; ulTopicCount < mqttTOPIC_COUNT; ulTopicCount++)
        {
            if( xTopicFilterContext[ ulTopicCount ].xSubAckStatus == MQTTSubAckFailure )
            {
                xFailedSubscribeToTopic = true;

                /* Generate a random number and calculate backoff value (in milliseconds) for
                 * the next connection retry.
                 * Note: It is recommended to seed the random number generator with a device-specific
                 * entropy source so that possibility of multiple devices retrying failed network operations
                 * at similar intervals can be avoided. */
                xBackoffAlgStatus = BackoffAlgorithm_GetNextBackoff( &xRetryParams, uxRand(), &usNextRetryBackOff );

                if( xBackoffAlgStatus == BackoffAlgorithmRetriesExhausted )
                {
                    LogError( ( "Server rejected subscription request. All retry attempts have exhausted. Topic=%s",
                                xTopicFilterContext[ ulTopicCount ].pcTopicFilter ) );
                }
                else if( xBackoffAlgStatus == BackoffAlgorithmSuccess )
                {
                    LogWarn( ( "Server rejected subscription request. Attempting to re-subscribe to topic %s.",
                               xTopicFilterContext[ ulTopicCount ].pcTopicFilter ) );
                    /* Backoff before the next re-subscribe attempt. */
                    vTaskDelay( pdMS_TO_TICKS( usNextRetryBackOff ) );
                }

                break;
            }
        }

        configASSERT( xBackoffAlgStatus != BackoffAlgorithmRetriesExhausted );
    } while( ( xFailedSubscribeToTopic == true ) && ( xBackoffAlgStatus == BackoffAlgorithmSuccess ) );
}

/*-----------------------------------------------------------*/

static void prvMQTTProcessResponse( MQTTPacketInfo_t * pxIncomingPacket,
                                    uint16_t usPacketId )
{
    uint32_t ulTopicCount = 0U;

    switch( pxIncomingPacket->type )
    {
        case MQTT_PACKET_TYPE_PUBACK:
            LogInfo( ( "PUBACK received for packet ID %u.\r\n", usPacketId ) );
            break;

        case MQTT_PACKET_TYPE_SUBACK:

            LogInfo( ( "SUBACK received for packet ID %u.", usPacketId ) );

            /* A SUBACK from the broker, containing the server response to our subscription request, has been received.
             * It contains the status code indicating server approval/rejection for the subscription to the single topic
             * requested. The SUBACK will be parsed to obtain the status code, and this status code will be stored in global
             * variable #xTopicFilterContext. */
            prvUpdateSubAckStatus( pxIncomingPacket );

		for (ulTopicCount = 0; ulTopicCount < mqttTOPIC_COUNT; ulTopicCount++)
            {
                if( xTopicFilterContext[ ulTopicCount ].xSubAckStatus != MQTTSubAckFailure )
                {
                    LogInfo( ( "Subscribed to the topic %s with maximum QoS %u.\r\n",
                               xTopicFilterContext[ ulTopicCount ].pcTopicFilter,
                               xTopicFilterContext[ ulTopicCount ].xSubAckStatus ) );
                }
            }

            /* Make sure ACK packet identifier matches with Request packet identifier. */
            configASSERT( usSubscribePacketIdentifier == usPacketId );
            break;

        case MQTT_PACKET_TYPE_UNSUBACK:
            LogInfo( ( "UNSUBACK received for packet ID %u.", usPacketId ) );
            /* Make sure ACK packet identifier matches with Request packet identifier. */
            configASSERT( usUnsubscribePacketIdentifier == usPacketId );
            break;

        case MQTT_PACKET_TYPE_PINGRESP:

            /* Nothing to be done from application as library handles
             * PINGRESP with the use of MQTT_ProcessLoop API function. */
            LogWarn( ( "PINGRESP should not be handled by the application "
                       "callback when using MQTT_ProcessLoop.\n" ) );
            break;

        case MQTT_PACKET_TYPE_PUBREC:
            LogInfo( ( "PUBREC received for packet id %u.\n\n",
                       usPacketId ) );
            break;

        case MQTT_PACKET_TYPE_PUBREL:

            /* Nothing to be done from application as library handles
             * PUBREL. */
            LogInfo( ( "PUBREL received for packet id %u.\n\n",
                       usPacketId ) );
            break;

        case MQTT_PACKET_TYPE_PUBCOMP:

            /* Nothing to be done from application as library handles
             * PUBCOMP. */
            LogInfo( ( "PUBCOMP received for packet id %u.\n\n",
                       usPacketId ) );
            break;

        /* Any other packet type is invalid. */
        default:
            LogWarn( ( "prvMQTTProcessResponse() called with unknown packet type:(%02X).\r\n",
                       pxIncomingPacket->type ) );
    }
}

/*-----------------------------------------------------------*/

static void prvMQTTProcessIncomingPublish( MQTTPublishInfo_t * pxPublishInfo )
{
    uint32_t ulTopicCount;
    BaseType_t xTopicFound = pdFALSE;

    configASSERT( pxPublishInfo != NULL );

    /* Process incoming Publish. */
    LogInfo( ( "Incoming QoS : %d\n", pxPublishInfo->qos ) );

    /* Verify the received publish is for one of the topics that's been subscribed to. */
	for (ulTopicCount = 0; ulTopicCount < ENDPOINT_COUNT; ulTopicCount++)
    {
		if (strncmp(endpoints[ulTopicCount].url, pxPublishInfo->pTopicName,
				pxPublishInfo->topicNameLength) == 0)
        {
			endpoints[ulTopicCount].handler(pxPublishInfo->pTopicName, pxPublishInfo->pPayload,
					pxPublishInfo->payloadLength);
			return;
        }
    }

	LogWarn(( "Received MQTT topic doesn't match any specified topics.\n"));
}

/*-----------------------------------------------------------*/

static void prvEventCallback( MQTTContext_t * pxMQTTContext,
                              MQTTPacketInfo_t * pxPacketInfo,
                              MQTTDeserializedInfo_t * pxDeserializedInfo )
{
    /* The MQTT context is not used in this function. */
    ( void ) pxMQTTContext;

    if( ( pxPacketInfo->type & 0xF0U ) == MQTT_PACKET_TYPE_PUBLISH )
    {
        LogInfo( ( "PUBLISH received for packet id %u.\n\n",
                   pxDeserializedInfo->packetIdentifier ) );
        prvMQTTProcessIncomingPublish( pxDeserializedInfo->pPublishInfo );
    }
    else
    {
        prvMQTTProcessResponse( pxPacketInfo, pxDeserializedInfo->packetIdentifier );
    }
}

/*-----------------------------------------------------------*/

static uint32_t prvGetTimeMs( void )
{
    TickType_t xTickCount = 0;
    uint32_t ulTimeMs = 0UL;

    /* Get the current tick count. */
    xTickCount = xTaskGetTickCount();

    /* Convert the ticks to milliseconds. */
    ulTimeMs = ( uint32_t ) xTickCount * MILLISECONDS_PER_TICK;

    /* Reduce ulGlobalEntryTimeMs from obtained time so as to always return the
     * elapsed time in the application. */
    ulTimeMs = ( uint32_t ) ( ulTimeMs - ulGlobalEntryTimeMs );

    return ulTimeMs;
}

/*-----------------------------------------------------------*/

static MQTTStatus_t prvProcessLoopWithTimeout( MQTTContext_t * pMqttContext,
                                               uint32_t ulTimeoutMs )
{
    uint32_t ulMqttProcessLoopTimeoutTime;
    uint32_t ulCurrentTime;

    MQTTStatus_t eMqttStatus = MQTTSuccess;

    ulCurrentTime = pMqttContext->getTime();
    ulMqttProcessLoopTimeoutTime = ulCurrentTime + ulTimeoutMs;

    /* Call MQTT_ProcessLoop multiple times a timeout happens, or
     * MQTT_ProcessLoop fails. */
    while( ( ulCurrentTime < ulMqttProcessLoopTimeoutTime ) &&
           ( eMqttStatus == MQTTSuccess || eMqttStatus == MQTTNeedMoreBytes ) )
    {
        eMqttStatus = MQTT_ProcessLoop( pMqttContext );
        ulCurrentTime = pMqttContext->getTime();
    }

    if( eMqttStatus == MQTTNeedMoreBytes )
    {
        eMqttStatus = MQTTSuccess;
    }

    return eMqttStatus;
}

/*-----------------------------------------------------------*/
