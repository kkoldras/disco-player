/**
 ******************************************************************************
 * @file    audioplayer_app.c
 * @author  MCD Application Team
 * @brief   Audio player application functions
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2016 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <flac_decoder.h>
#include <mp3_decoder.h>
#include <playlist_explorer.h>
#include <wav_decoder.h>
#include "audio_player_app.h"
#include "ff_stdio.h"
#include "stm32746g_discovery_audio.h"

/** @addtogroup AUDIO_PLAYER_MODULE
 * @{
 */

/** @defgroup AUDIO_APPLICATION
 * @brief audio application routines
 * @{
 */

/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
// #define PLAYER_DEBUG(x) printf x
#define PLAYER_DEBUG(x) while (0) {}
//#define PLAYER_DEBUG(x) SEGGER_SYSVIEW_PrintfTarget x

/* Private variables ---------------------------------------------------------*/
static FF_FILE* audio_file;

static osMailQId AudioEvent = 0;
static osThreadId AudioThreadId = 0;

/* Private function prototypes -----------------------------------------------*/
static void Audio_Thread(void const* argument);
static void AUDIO_TransferComplete_CallBack(void);
static void AUDIO_HalfTransfer_CallBack(void);
static void AUDIO_Error_CallBack(void);

static uint8_t allowReading = 0;
static AudioDecoder* drv;
static AudioDecoder* drvs[DECODER_COUNT];
int drv_type = -1;

typedef enum {
	TRANSFER_HALF = 0,
	TRANSFER_COMPLETE = 1,
	PLAY_CMD,
	STOP_CMD,
	PAUSE_CMD,
	RESUME_CMD,
	SET_POSITION_CMD,
	SELECT_FILE_CMD,
} PlayerEventType;

typedef struct {
	PlayerEventType type;
	union {
		void* ptr;
		uint32_t data;
	};
} PlayerEvent;

/* Private functions ---------------------------------------------------------*/

static int GetDecoderTypeByFilename(char* filename) {
	if (is_end_with(filename, ".wav") || is_end_with(filename, ".WAV")) {
		return DECODER_WAV;
	}
	if (is_end_with(filename, ".flac") || is_end_with(filename, ".FLAC")) {
		return DECODER_FLAC;
	}
	if (is_end_with(filename, ".mp3") || is_end_with(filename, ".MP3")) {
		return DECODER_MP3;
	}

	return -1;

}

static void _Play(uint32_t frequency) {
	uint32_t numOfReadBytes;
	haudio.out.state = AUDIOPLAYER_PLAY;
	if (audio_file == NULL) {
		PLAYER_DEBUG(("Audio: File not selected\n"));
		return;
	}

	/* Fill whole buffer @ first time */
	numOfReadBytes = drv->read_file_fn(&haudio.buff[0], AUDIO_OUT_BUFFER_SIZE, audio_file);
	PLAYER_DEBUG(("Audio: Play, Filled completely, size: %d\n", numOfReadBytes));
	if (numOfReadBytes > 0) {
		BSP_AUDIO_OUT_Pause();
		BSP_AUDIO_OUT_SetFrequency(frequency);
		PLAYER_DEBUG(("Audio: Play, setting frequency: %d\n", frequency));
		BSP_AUDIO_OUT_Play((uint16_t*)&haudio.buff[0], numOfReadBytes);
		allowReading = 1;
	}
}

static void _Stop(void) {
	haudio.out.state = AUDIOPLAYER_STOP;
	BSP_AUDIO_OUT_Stop(CODEC_PDWN_SW);

	if (AudioThreadId != 0) {
		allowReading = 0;
		PLAYER_DEBUG(("Audio: Stop\n"));
	}
	ff_fclose(audio_file);
}

static void _Pause(void) {
	haudio.out.state = AUDIOPLAYER_PAUSE;
	if (AudioThreadId != 0) {
		allowReading = 0;
		PLAYER_DEBUG(("Audio: Pause\n"));
	}
	BSP_AUDIO_OUT_Pause();
}

static void _Resume(void) {
	haudio.out.state = AUDIOPLAYER_PLAY;
	if (AudioThreadId != 0) {
		allowReading = 1;
		PLAYER_DEBUG(("Audio: Resume\n"));
	}
	BSP_AUDIO_OUT_Resume();
}

static void _SetPosition(uint32_t position) {
	if (!drv->set_position_fn(audio_file, position)) {
		PLAYER_DEBUG(("AUDIO: Could not set position %d\n", position));
	}
}

static void _SelectFile(char* file) {
	audio_file = ff_fopen(file, "r+");

	if (audio_file != NULL) {
		// Not enough heap to fit 2 decoders at a time (e.g. FLAC & MP3) so unload current one
		int type = GetDecoderTypeByFilename(file);
		if (type != drv_type && drv != NULL) {
			drv->deinit_fn();
			drv = NULL;
			drv_type = -1;
		}

		if (drv == NULL) {
			for (int i = 0; i < DECODER_COUNT; i++) {
				if (drvs[i]->get_file_info_fn(audio_file, NULL)) {
					drv = drvs[i];
					drv_type = i;
					break;
				}
			}
		}
		if (drv != NULL) {
			drv->init_fn(audio_file);
		}
	}
}


/**
 * @brief  Initializes audio
 * @param  None.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_Init(uint8_t volume) {
	/* Try to Init Audio interface in diffrent config in case of failure */
	BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_AUTO, volume, SAI_AUDIO_FREQUENCY_48K);
	BSP_AUDIO_OUT_SetAudioFrameSlot(CODEC_AUDIOFRAME_SLOT_02);

	/* Initialize internal audio structure */
	haudio.out.state = AUDIOPLAYER_STOP;
	haudio.out.mute = MUTE_OFF;
	haudio.out.volume = volume;

	drv = &wav_decoder;
	drvs[DECODER_WAV] = &wav_decoder;
	drvs[DECODER_FLAC] = &flac_decoder;
	drvs[DECODER_MP3] = &mp3_decoder;

	/* Register audio BSP drivers callbacks */
	AUDIO_IF_RegisterCallbacks(
		AUDIO_TransferComplete_CallBack,
		AUDIO_HalfTransfer_CallBack,
		AUDIO_Error_CallBack
	);

	/* Create Audio Queue */
	osMailQDef(AUDIO_Queue, 10, PlayerEvent);
	AudioEvent = osMailCreate(osMailQ(AUDIO_Queue), NULL);

	/* Create Audio task */
	osThreadDef(osAudio_Thread, Audio_Thread, osPriorityRealtime, 0, 5120);
	AudioThreadId = osThreadCreate(osThread(osAudio_Thread), NULL);

	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Get audio state
 * @param  None.
 * @retval Audio state.
 */
AUDIOPLAYER_StateTypdef AUDIOPLAYER_GetState(void) { return haudio.out.state; }

/**
 * @brief  Get audio volume
 * @param  None.
 * @retval Audio volume.
 */
uint32_t AUDIOPLAYER_GetVolume(void) { return haudio.out.volume; }

/**
 * @brief  Set audio volume
 * @param  Volume: Volume level to be set in percentage from 0% to 100% (0 for
 *         Mute and 100 for Max volume level).
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_SetVolume(uint32_t volume) {
	if (BSP_AUDIO_OUT_SetVolume(volume) == AUDIO_OK) {
		haudio.out.volume = volume;
		return AUDIOPLAYER_ERROR_NONE;
	} else {
		return AUDIOPLAYER_ERROR_HW;
	}
}

/**
 * @brief  Play audio stream
 * @param  frequency: Audio frequency used to play the audio stream.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_Play(uint32_t frequency) {
	PlayerEvent* e = osMailAlloc(AudioEvent, 0);
	if (e == NULL) {
		return AUDIOPLAYER_ERROR_MEM;
	}

	e->type = PLAY_CMD;
	e->data = frequency;

	if (osMailPut(AudioEvent, e) != osOK) {
		return AUDIOPLAYER_ERROR_IO;
	}
	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Audio player process
 * @param  None.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_Process(void) {
	switch (haudio.out.state) {
		case AUDIOPLAYER_START:
			haudio.out.state = AUDIOPLAYER_PLAY;
			break;

		case AUDIOPLAYER_EOF:
			haudio.out.state = AUDIOPLAYER_EOF;
			AUDIOPLAYER_NotifyEndOfFile();
			break;

		case AUDIOPLAYER_ERROR:
			AUDIOPLAYER_Stop();
			break;

		case AUDIOPLAYER_STOP:
		case AUDIOPLAYER_PLAY:
		default:
			break;
	}

	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Audio player DeInit
 * @param  None.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_DeInit(void) {
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

	haudio.out.state = AUDIOPLAYER_STOP;

	BSP_AUDIO_OUT_Stop(CODEC_PDWN_HW);
	BSP_AUDIO_OUT_DeInit();

	ff_fclose(audio_file);

	if (AudioEvent != 0) {
//		There is a memleak here, but this cannot be solved without switching to CMSIS v2 or FreeRTOS directly.
//		vPortFree(AudioEvent->pool);
//		vQueueDelete(AudioEvent->handle);
		AudioEvent = 0;
	}
	if (AudioThreadId != 0) {
		osThreadTerminate(AudioThreadId);
		AudioThreadId = 0;
	}

	/* Restore SAI PLL clock */
	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC;
	PeriphClkInitStruct.PLLSAI.PLLSAIN = 192;
	PeriphClkInitStruct.PLLSAI.PLLSAIR = 5;
	PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_4;
	HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);

	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Stop audio stream.
 * @param  None.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_Stop(void) {
	PlayerEvent* e = osMailAlloc(AudioEvent, 0);
	if (e == NULL) {
		return AUDIOPLAYER_ERROR_MEM;
	}

	e->type = STOP_CMD;

	if (osMailPut(AudioEvent, e) != osOK) {
		return AUDIOPLAYER_ERROR_IO;
	}
	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Pause Audio stream
 * @param  None.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_Pause(void) {
	PlayerEvent* e = osMailAlloc(AudioEvent, 0);
	if (e == NULL) {
		return AUDIOPLAYER_ERROR_MEM;
	}

	e->type = PAUSE_CMD;

	if (osMailPut(AudioEvent, e) != osOK) {
		return AUDIOPLAYER_ERROR_IO;
	}
	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Resume Audio stream
 * @param  None.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_Resume(void) {
	PlayerEvent* e = osMailAlloc(AudioEvent, 0);
	if (e == NULL) {
		return AUDIOPLAYER_ERROR_MEM;
	}

	e->type = RESUME_CMD;

	if (osMailPut(AudioEvent, e) != osOK) {
		return AUDIOPLAYER_ERROR_IO;
	}
	return AUDIOPLAYER_ERROR_NONE;

}

/**
 * @brief  Sets audio stream position
 * @param  position: stream position.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_SetPosition(uint32_t position) {
	PlayerEvent* e = osMailAlloc(AudioEvent, 0);
	if (e == NULL) {
		return AUDIOPLAYER_ERROR_MEM;
	}

	e->type = SET_POSITION_CMD;
	e->data = position;

	if (osMailPut(AudioEvent, e) != osOK) {
		return AUDIOPLAYER_ERROR_IO;
	}
	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Sets the volume at mute
 * @param  state: could be MUTE_ON to mute sound or MUTE_OFF to unmute
 *                the codec and restore previous volume level.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_Mute(uint8_t state) {
	BSP_AUDIO_OUT_SetMute(state);

	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Get the wav file information.
 * @param  file: wav file.
 * @param  info: pointer to wav file structure
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_GetFileInfo(char* file, Audio_InfoTypedef* info) {
	AUDIOPLAYER_ErrorTypdef ret = AUDIOPLAYER_ERROR_IO;
	FF_FILE* fsfile;

	fsfile = ff_fopen(file, "r+");

	if (fsfile != NULL) {
		// Not enough heap to fit 2 decoders at a time (e.g. FLAC & MP3) so unload current one
		int type = GetDecoderTypeByFilename(file);
		if (type != drv_type && drv != NULL) {
			drv->deinit_fn();
			drv = NULL;
			drv_type = -1;
		}

		for (int i = 0; i < DECODER_COUNT; i++) {
			if (drvs[i]->get_file_info_fn(fsfile, info)) {
				ret = AUDIOPLAYER_ERROR_NONE;
				break;
			}
		}

		ff_fclose(fsfile);
	}
	return ret;
}

/**
 * @brief  Select wav file.
 * @param  file: wav file.
 * @retval Audio state.
 */
AUDIOPLAYER_ErrorTypdef AUDIOPLAYER_SelectFile(char* file) {
	PlayerEvent* e = osMailAlloc(AudioEvent, 0);
	if (e == NULL) {
		return AUDIOPLAYER_ERROR_MEM;
	}

	e->type = SELECT_FILE_CMD;
	e->ptr = file;

	if (osMailPut(AudioEvent, e) != osOK) {
		return AUDIOPLAYER_ERROR_IO;
	}
	return AUDIOPLAYER_ERROR_NONE;
}

/**
 * @brief  Get wav file progress.
 * @param  None
 * @retval file progress.
 */
uint32_t AUDIOPLAYER_GetProgress(void) { return ff_ftell(audio_file); }

/**
 * @brief  Manages the DMA Transfer complete interrupt.
 * @param  None
 * @retval None
 */
static void AUDIO_TransferComplete_CallBack(void) {
	if (haudio.out.state == AUDIOPLAYER_PLAY) {
		BSP_AUDIO_OUT_ChangeBuffer((uint16_t*)&haudio.buff[0], AUDIO_OUT_BUFFER_SIZE / 2);
		PLAYER_DEBUG(("Audio: Full transfer, swapped buffers\n"));
		memset(&haudio.buff[AUDIO_OUT_BUFFER_SIZE / 2], '\0', AUDIO_OUT_BUFFER_SIZE / 2);
		PlayerEvent* ev = osMailAlloc(AudioEvent, 0);
		ev->type = TRANSFER_COMPLETE;
		osMailPut(AudioEvent, ev);
		//osMessagePut(AudioEvent, PLAY_BUFFER_OFFSET_FULL, 0);
	}
}

/**
 * @brief  Manages the DMA Half Transfer complete interrupt.
 * @param  None
 * @retval None
 */
static void AUDIO_HalfTransfer_CallBack(void) {
	if (haudio.out.state == AUDIOPLAYER_PLAY) {
		BSP_AUDIO_OUT_ChangeBuffer(
			(uint16_t*)&haudio.buff[AUDIO_OUT_BUFFER_SIZE / 2],
			AUDIO_OUT_BUFFER_SIZE / 2
		);
		PLAYER_DEBUG(("Audio: Half transfer, swapped buffers\n"));
		memset(&haudio.buff[0], '\0', AUDIO_OUT_BUFFER_SIZE / 2);
		PlayerEvent* ev = osMailAlloc(AudioEvent, 0);
		ev->type = TRANSFER_HALF;
		osMailPut(AudioEvent, ev);
		//osMessagePut(AudioEvent, PLAY_BUFFER_OFFSET_HALF, 0);
	}
}

/**
 * @brief  Manages the DMA FIFO error interrupt.
 * @param  None
 * @retval None
 */
static void AUDIO_Error_CallBack(void) {
	PLAYER_DEBUG(("Audio ERROR\n"));
	//haudio.out.state = AUDIOPLAYER_ERROR;
}

/**
 * @brief  Audio task
 * @param  argument: pointer that is passed to the thread function as start argument.
 * @retval None
 */
static void Audio_Thread(void const* argument) {
	uint32_t numOfReadBytes;
	osEvent event;
	for (;;) {
		event = osMailGet(AudioEvent, 100);
		if (event.status != osEventMail) {
			continue;
		}

		PlayerEvent* info = event.value.p;
		switch (info->type) {
			case PLAY_CMD:
				_Play(info->data);
				break;
			case STOP_CMD:
				_Stop();
				break;
			case PAUSE_CMD:
				_Pause();
				break;
			case RESUME_CMD:
				_Resume();
				break;
			case SET_POSITION_CMD:
				_SetPosition(info->data);
				break;
			case SELECT_FILE_CMD:
				_SelectFile(info->ptr);
				break;
			default:
				break;
		}

		if (allowReading && haudio.out.state == AUDIOPLAYER_PLAY && (info->type & 0xFE) == 0) {
			uint8_t* dstBuffer = info->type == TRANSFER_COMPLETE
							   ? &haudio.buff[AUDIO_OUT_BUFFER_SIZE / 2]
							   : &haudio.buff[0];

			numOfReadBytes = drv->read_file_fn(dstBuffer, AUDIO_OUT_BUFFER_SIZE / 2, audio_file);

			if (numOfReadBytes == 0) {
				if (ff_feof(audio_file)) {
					haudio.out.state = AUDIOPLAYER_EOF;
				} else {
					haudio.out.state = AUDIOPLAYER_ERROR;
				}
			}
		}
		osMailFree(AudioEvent, info);
	}
}

/**
 * @}
 */

/**
 * @}
 */
