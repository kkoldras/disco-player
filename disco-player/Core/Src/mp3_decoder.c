/*
 * mp3_decoder.c
 */

#define MINIMP3_IMPLEMENTATION
#define MINIMP3_NO_STDIO
#include <mp3_decoder.h>
#include "SEGGER_SYSVIEW.h"
#include "minimp3_ex.h"

static mp3dec_ex_t* decoder = NULL;

size_t read_cb(void *buf, size_t size, void *user_data) {
	FF_FILE* file = (FF_FILE*)user_data;
	if (file == NULL) {
		return 0;
	}

	return ff_fread(buf, 1, size, file);
}

int seek_cb(uint64_t position, void *user_data) {
	FF_FILE* file = (FF_FILE*)user_data;
	if (file == NULL) {
		return -1;
	}

	return ff_fseek(file, position, FF_SEEK_SET);
}

static mp3dec_io_t callbacks = {
		.read = read_cb,
		.read_data = NULL,
		.seek = seek_cb,
		.seek_data = NULL
};

static void set_current_file(FF_FILE* file) {
	callbacks.read_data = file;
	callbacks.seek_data = file;
}

size_t mp3_read_file(void* dstBuffer, size_t size, FF_FILE* file) {
	set_current_file(file);

	return mp3dec_ex_read(decoder, dstBuffer, size / 2) * 2;
}

uint8_t mp3_set_position(FF_FILE* file, uint8_t percentage) {
	long file_pos;
	size_t file_len = ff_filelength(file);

	file_pos = file_len / AUDIO_OUT_BUFFER_SIZE / 100;
	file_pos *= (percentage * AUDIO_OUT_BUFFER_SIZE);
	file_pos = (file_len <= file_pos) ? file_len : file_pos;
	if (ff_fseek(file, file_pos, FF_SEEK_SET) == -1) {
		return 0;
	}

	return 1;
}

uint8_t mp3_get_file_info(FF_FILE* file, Audio_InfoTypedef* info) {
	long tmpPos = ff_ftell(file);
	if (ff_fseek(file, 0, FF_SEEK_SET) != 0) {
		return 0;
	}

	size_t buf_size = 16*1024;
	uint8_t* buf = malloc(buf_size);

	set_current_file(file);

	int res = mp3dec_detect_cb(&callbacks, buf, buf_size);
	if (res) {
		SEGGER_SYSVIEW_PrintfHost("MP3 detect failed. Code %d", res);
	}
	free(buf);

	mp3dec_ex_t* decoder = malloc(sizeof(mp3dec_ex_t));
	res = mp3dec_ex_open_cb(decoder, &callbacks, 0);
	if (res) {
		SEGGER_SYSVIEW_PrintfHost("Can't init MP3 decoder to get file info. Code %d", res);
	} else {
		info->Duration = decoder->samples / decoder->info.channels / decoder->info.hz;
		info->SampleRate = decoder->info.hz;
		info->SampleSize = 16;
	}


	mp3dec_ex_close(decoder);
	free(decoder);
	decoder = NULL;

	set_current_file(NULL);

	ff_fseek(file, tmpPos, FF_SEEK_SET);
	return res ? 0 : 1;
}

void mp3_init(FF_FILE* file) {
	if (decoder == NULL) {
		decoder = malloc(sizeof(mp3dec_ex_t));
	}

	set_current_file(file);

	int res = mp3dec_ex_open_cb(decoder, &callbacks, 0);
	if (res) {
		SEGGER_SYSVIEW_PrintfHost("Can't init MP3 decoder. Code %d", res);
	}
}

static void mp3_deinit(void) {
	set_current_file(NULL);
	if (decoder != NULL) {
		mp3dec_ex_close(decoder);
		free(decoder);
		decoder = NULL;
	}
}

AudioDecoder mp3_decoder = {
	.get_file_info_fn = mp3_get_file_info,
	.read_file_fn = mp3_read_file,
	.set_position_fn = mp3_set_position,
	.init_fn = mp3_init,
	.deinit_fn = mp3_deinit
};
