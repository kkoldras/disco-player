/**
 * @file playlist_explorer.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include <playlist_explorer.h>

/*********************
 *      DEFINES
 *********************/
#define MY_CLASS &playlist_explorer_class

#define COL_TITLE 0
#define COL_TYPE 1
#define COL_STATUS 2

#define DIR_BACK "Back"

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/
static void playlist_explorer_constructor(const lv_obj_class_t* class_p, lv_obj_t* obj);

static void browser_file_event_handler(lv_event_t* e);

static void init_style(lv_obj_t* obj);
static void show_dir(lv_obj_t* obj, const char* path);
static void show_playlist(lv_obj_t *obj, const char *path);
static void strip_ext(char* dir);
static void file_explorer_sort(lv_obj_t* obj);
static void sort_by_file_kind(lv_obj_t* tb, int16_t lo, int16_t hi);
static void exch_table_item(lv_obj_t* tb, int16_t i, int16_t j);

/**********************
 *  STATIC VARIABLES
 **********************/

const lv_obj_class_t playlist_explorer_class = {
	.constructor_cb = playlist_explorer_constructor,
	.width_def = LV_SIZE_CONTENT,
	.height_def = LV_SIZE_CONTENT,
	.instance_size = sizeof(playlist_explorer_t),
	.base_class = &lv_obj_class
};

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/

lv_obj_t* playlist_explorer_create(lv_obj_t* parent) {
	LV_LOG_INFO("begin");
	lv_obj_t* obj = lv_obj_class_create_obj(MY_CLASS, parent);
	lv_obj_class_init_obj(obj);
	return obj;
}

/*=====================
 * Setter functions
 *====================*/
void playlist_explorer_set_sort(lv_obj_t* obj, playlist_explorer_sort_t sort) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	explorer->sort = sort;

	file_explorer_sort(obj);
}

/*=====================
 * Getter functions
 *====================*/
const char* playlist_explorer_get_selected_file_name(const lv_obj_t* obj) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	return explorer->sel_fn;
}

const char* playlist_explorer_get_current_path(const lv_obj_t* obj) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	return explorer->current_path;
}

lv_obj_t* playlist_explorer_get_file_table(lv_obj_t* obj) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	return explorer->file_table;
}

lv_obj_t* playlist_explorer_get_header(lv_obj_t* obj) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	return explorer->head_area;
}

lv_obj_t* playlist_explorer_get_path_label(lv_obj_t* obj) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	return explorer->path_label;
}

playlist_explorer_sort_t playlist_explorer_get_sort(const lv_obj_t* obj) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	return explorer->sort;
}

/*=====================
 * Other functions
 *====================*/
void playlist_explorer_open_dir(lv_obj_t* obj, const char* dir) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	show_dir(obj, dir);
}

/**********************
 *   STATIC FUNCTIONS
 **********************/
static void playlist_explorer_constructor(const lv_obj_class_t* class_p, lv_obj_t* obj) {
	LV_UNUSED(class_p);
	LV_TRACE_OBJ_CREATE("begin");

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	explorer->sort = LV_EXPLORER_SORT_NONE;

	lv_memset_00(explorer->current_path, sizeof(explorer->current_path));

	lv_obj_set_size(obj, LV_PCT(100), LV_PCT(100));
	lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_COLUMN);

	explorer->cont = lv_obj_create(obj);
	lv_obj_set_width(explorer->cont, LV_PCT(100));
	lv_obj_set_flex_grow(explorer->cont, 1);

	/*File table area on the right*/
	explorer->browser_area = lv_obj_create(explorer->cont);

	lv_obj_set_size(explorer->browser_area, LV_PCT(100), LV_PCT(100));
	lv_obj_set_flex_flow(explorer->browser_area, LV_FLEX_FLOW_COLUMN);

	/*The area displayed above the file browse list(head)*/
	explorer->head_area = lv_obj_create(explorer->browser_area);
	lv_obj_set_size(explorer->head_area, LV_PCT(100), LV_PCT(14));
	lv_obj_clear_flag(explorer->head_area, LV_OBJ_FLAG_SCROLLABLE);

	/*Show current path*/
	explorer->path_label = lv_label_create(explorer->head_area);
	lv_label_set_text(explorer->path_label, "Explorer");
	lv_obj_center(explorer->path_label);

	/*Table showing the contents of the table of contents*/
	explorer->file_table = lv_table_create(explorer->browser_area);
	lv_obj_set_size(explorer->file_table, LV_PCT(100), LV_PCT(86));
	lv_table_set_col_width(explorer->file_table, COL_TITLE, LV_PCT(100));
	lv_table_set_col_cnt(explorer->file_table, COL_TYPE);
	lv_obj_add_event_cb(explorer->file_table, browser_file_event_handler, LV_EVENT_ALL, obj);

	/*only scroll up and down*/
	lv_obj_set_scroll_dir(explorer->file_table, LV_DIR_TOP | LV_DIR_BOTTOM);

	/*Initialize style*/
	init_style(obj);

	explorer->m3u_state = lv_mem_alloc(sizeof(m3u_state));

	LV_TRACE_OBJ_CREATE("finished");
}

static void init_style(lv_obj_t* obj) {
	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	/*playlist_explorer obj style*/
	lv_obj_set_style_radius(obj, 0, 0);
	lv_obj_set_style_bg_color(obj, lv_color_hex(0xf2f1f6), 0);

	/*main container style*/
	lv_obj_set_style_radius(explorer->cont, 0, 0);
	lv_obj_set_style_bg_opa(explorer->cont, LV_OPA_0, 0);
	lv_obj_set_style_border_width(explorer->cont, 0, 0);
	lv_obj_set_style_outline_width(explorer->cont, 0, 0);
	lv_obj_set_style_pad_column(explorer->cont, 0, 0);
	lv_obj_set_style_pad_row(explorer->cont, 0, 0);
	lv_obj_set_style_flex_flow(explorer->cont, LV_FLEX_FLOW_ROW, 0);
	lv_obj_set_style_pad_all(explorer->cont, 0, 0);
	lv_obj_set_style_layout(explorer->cont, LV_LAYOUT_FLEX, 0);

	/*head cont style*/
	lv_obj_set_style_radius(explorer->head_area, 0, 0);
	lv_obj_set_style_border_width(explorer->head_area, 0, 0);
	lv_obj_set_style_pad_top(explorer->head_area, 0, 0);

	/*File browser container style*/
	lv_obj_set_style_pad_all(explorer->browser_area, 0, 0);
	lv_obj_set_style_pad_row(explorer->browser_area, 0, 0);
	lv_obj_set_style_radius(explorer->browser_area, 0, 0);
	lv_obj_set_style_border_width(explorer->browser_area, 0, 0);
	lv_obj_set_style_outline_width(explorer->browser_area, 0, 0);
	lv_obj_set_style_bg_color(explorer->browser_area, lv_color_hex(0xffffff), 0);

	/*Style of the table in the browser container*/
	lv_obj_set_style_bg_color(explorer->file_table, lv_color_hex(0xffffff), 0);
	lv_obj_set_style_pad_all(explorer->file_table, 0, 0);
	lv_obj_set_style_radius(explorer->file_table, 0, 0);
	lv_obj_set_style_border_width(explorer->file_table, 0, 0);
	lv_obj_set_style_outline_width(explorer->file_table, 0, 0);
}

static void browser_file_event_handler(lv_event_t* e) {
	lv_event_code_t code = lv_event_get_code(e);
	lv_obj_t* obj = lv_event_get_user_data(e);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	if (code == LV_EVENT_VALUE_CHANGED) {
		char file_name[PLAYLIST_EXPLORER_PATH_MAX_LEN];
		const char* str_fn = NULL;
		uint16_t row;
		uint16_t col;

		lv_memset_00(file_name, sizeof(file_name));
		lv_table_get_selected_cell(explorer->file_table, &row, &col);
		str_fn = lv_table_get_cell_value(explorer->file_table, row, COL_TITLE);

		str_fn = str_fn + 5;
		if ((strcmp(str_fn, ".") == 0)) {
			return;
		}

		if ((strcmp(str_fn, DIR_BACK) == 0) && (strlen(explorer->current_path) > 3)) {
			strip_ext(explorer->current_path);
			/*Remove the last '/' character*/
			strip_ext(explorer->current_path);
			lv_snprintf((char*)file_name, sizeof(file_name), "%s", explorer->current_path);
		} else {
			if (strcmp(str_fn, DIR_BACK) != 0) {
				lv_snprintf((char*)file_name, sizeof(file_name), "%s%s", explorer->current_path, str_fn);
			}
		}

		lv_fs_dir_t dir;
		if (lv_fs_dir_open(&dir, file_name) == LV_FS_RES_OK) {
			lv_fs_dir_close(&dir);
			show_dir(obj, (char*)file_name);
		} else {
			if ((is_end_with(str_fn, ".m3u") == true) || (is_end_with(str_fn, ".M3U") == true)) {
				show_playlist(obj, (char*) file_name);
			}
			if (strcmp(str_fn, DIR_BACK) != 0) {
				explorer->sel_fn = str_fn;
				lv_event_send(obj, LV_EVENT_VALUE_CHANGED, NULL);
			}
		}
	} else if (code == LV_EVENT_SIZE_CHANGED) {
		lv_table_set_col_width(explorer->file_table, 0, lv_obj_get_width(explorer->file_table));
	} else if ((code == LV_EVENT_CLICKED) || (code == LV_EVENT_RELEASED)) {
		lv_event_send(obj, LV_EVENT_CLICKED, NULL);
	}
}

static uint8_t is_hidden(void* dir_d) {
	return ((FF_FindData_t*)dir_d)->ucAttributes & FF_FAT_ATTR_HIDDEN;
}

static void show_dir(lv_obj_t* obj, const char* path) {
	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	char fn[PLAYLIST_EXPLORER_PATH_MAX_LEN];
	uint16_t index = 0;
	lv_fs_dir_t dir;
	lv_fs_res_t res;

	res = lv_fs_dir_open(&dir, path);
	if (res != LV_FS_RES_OK) {
		LV_LOG_USER("Open dir error %d!", res);
		return;
	}

	const char *real_path = path + 1; /*Ignore the driver letter*/
	if (*real_path == ':') {
		real_path++;
	}

	if (strlen(real_path) > 0 && strcmp(real_path, "/") != 0) {
		lv_table_set_cell_value_fmt(explorer->file_table, index++, COL_TITLE,
		LV_SYMBOL_LEFT "  %s", "Back");
		lv_table_set_cell_value(explorer->file_table, 0, COL_TYPE, "0");
	}

	while (1) {
		uint8_t fn_hidden = is_hidden(dir.dir_d);
		res = lv_fs_dir_read(&dir, fn);
		if (res != LV_FS_RES_OK) {
			LV_LOG_USER("Driver, file or directory is not exists %d!", res);
			break;
		}

		if (fn_hidden) {
			continue;
		}

		/*fn is empty, if not more files to read*/
		if (strlen(fn) == 0) {
			LV_LOG_USER("Not more files to read!");
			break;
		}

		if ((is_end_with(fn, ".mp3") == true) || (is_end_with(fn, ".MP3") == true)
				|| (is_end_with(fn, ".wav") == true) || (is_end_with(fn, ".WAV") == true)
				|| (is_end_with(fn, ".flac") == true) || (is_end_with(fn, ".FLAC") == true)) {
			lv_table_set_cell_value_fmt(explorer->file_table, index, COL_TITLE,
					LV_SYMBOL_AUDIO "  %s", fn);
			lv_table_set_cell_value(explorer->file_table, index, COL_TYPE, "2");
		} else if ((is_end_with(fn, ".m3u") == true) || (is_end_with(fn, ".M3U") == true)) {
			lv_table_set_cell_value_fmt(explorer->file_table, index, COL_TITLE,
					LV_SYMBOL_LIST "  %s", fn);
			lv_table_set_cell_value(explorer->file_table, index, COL_TYPE, "8");
		} else if ((is_end_with(fn, ".") == true) || (is_end_with(fn, DIR_BACK) == true)) {
			/*is dir*/
			continue;
		} else if (fn[0] == '/') { /*is dir*/
			lv_table_set_cell_value_fmt(
					explorer->file_table,
					index,
					COL_TITLE,
					LV_SYMBOL_LIST "  %s",
					fn + 1
							);
			lv_table_set_cell_value(explorer->file_table, index, COL_TYPE, "0");
		} else {
			continue;
//			lv_table_set_cell_value_fmt(explorer->file_table, index, 0, LV_SYMBOL_FILE "  %s", fn);
//			lv_table_set_cell_value(explorer->file_table, index, 1, "4");
		}

		index++;
	}

	lv_fs_dir_close(&dir);

	lv_table_set_row_cnt(explorer->file_table, index);
	file_explorer_sort(obj);
	lv_event_send(obj, LV_EVENT_READY, NULL);

	/*Move the table to the top*/
	lv_obj_scroll_to_y(explorer->file_table, 0, LV_ANIM_OFF);

	lv_memset_00(explorer->current_path, sizeof(explorer->current_path));
	strncpy(explorer->current_path, path, sizeof(explorer->current_path) - 1);
	lv_label_set_text_fmt(explorer->path_label, LV_SYMBOL_EYE_OPEN " %s", path);

	size_t current_path_len = strlen(explorer->current_path);
	if ((*((explorer->current_path) + current_path_len) != '/')
			&& (current_path_len < PLAYLIST_EXPLORER_PATH_MAX_LEN)) {
		*((explorer->current_path) + current_path_len) = '/';
	}
}

static void show_playlist(lv_obj_t *obj, const char *path) {
	playlist_explorer_t *explorer = (playlist_explorer_t*) obj;

	uint16_t index = 0;
	int res;

	char* real_path = (char*) path + 1; /*Ignore the driver letter*/
	if (*real_path == ':') {
		real_path++;
	}

	m3u_open(real_path, explorer->m3u_state);

	if (explorer->m3u_state->file == NULL) {
		LV_LOG_USER("Open M3U error!");
		return;
	}



	if (strlen(real_path) > 0 && strcmp(real_path, "/") != 0) {
		lv_table_set_cell_value_fmt(explorer->file_table, index++, 0, LV_SYMBOL_LEFT "  %s",
		DIR_BACK);
		lv_table_set_cell_value(explorer->file_table, 0, 1, "0");
	}

	while (1) {

		res = m3u_next(explorer->m3u_state);
		if (res == 0) {
			LV_LOG_USER("Could not read M3U entry!");
			break;
		}

		char* fn = explorer->m3u_state->filename;

		if ((is_end_with(fn, ".mp3") == true) || (is_end_with(fn, ".MP3") == true)
				|| (is_end_with(fn, ".wav") == true) || (is_end_with(fn, ".WAV") == true)
				|| (is_end_with(fn, ".flac") == true) || (is_end_with(fn, ".FLAC") == true)) {
			lv_table_set_cell_value_fmt(explorer->file_table, index, 0, LV_SYMBOL_AUDIO "  %s", fn);
			lv_table_set_cell_value(explorer->file_table, index, 1, "2");
		} else {
			continue;
		}

		index++;
	}

	m3u_close(explorer->m3u_state);

	lv_table_set_row_cnt(explorer->file_table, index);
	file_explorer_sort(obj);
	lv_event_send(obj, LV_EVENT_READY, NULL);

	/*Move the table to the top*/
	lv_obj_scroll_to_y(explorer->file_table, 0, LV_ANIM_OFF);

	lv_memset_00(explorer->current_path, sizeof(explorer->current_path));
	strncpy(explorer->current_path, path, sizeof(explorer->current_path) - 1);
	lv_label_set_text_fmt(explorer->path_label, LV_SYMBOL_EYE_OPEN " %s", path);

	size_t current_path_len = strlen(explorer->current_path);
	if ((*((explorer->current_path) + current_path_len) != '/') && (current_path_len < PLAYLIST_EXPLORER_PATH_MAX_LEN)) {
		*((explorer->current_path) + current_path_len) = '/';
	}
}

/*Remove the specified suffix*/
static void strip_ext(char* dir) {
	char* end = dir + strlen(dir);

	while (end >= dir && *end != '/') {
		--end;
	}

	if (end > dir) {
		*end = '\0';
	} else if (end == dir) {
		*(end + 1) = '\0';
	}
}

static void exch_table_item(lv_obj_t* tb, int16_t i, int16_t j) {
	const char* tmp;
	tmp = lv_table_get_cell_value(tb, i, 0);
	lv_table_set_cell_value(tb, 0, 2, tmp);
	lv_table_set_cell_value(tb, i, 0, lv_table_get_cell_value(tb, j, 0));
	lv_table_set_cell_value(tb, j, 0, lv_table_get_cell_value(tb, 0, 2));

	tmp = lv_table_get_cell_value(tb, i, 1);
	lv_table_set_cell_value(tb, 0, 2, tmp);
	lv_table_set_cell_value(tb, i, 1, lv_table_get_cell_value(tb, j, 1));
	lv_table_set_cell_value(tb, j, 1, lv_table_get_cell_value(tb, 0, 2));
}

static void file_explorer_sort(lv_obj_t* obj) {
	LV_ASSERT_OBJ(obj, MY_CLASS);

	playlist_explorer_t* explorer = (playlist_explorer_t*)obj;

	uint16_t sum = lv_table_get_row_cnt(explorer->file_table);

	if (sum > 1) {
		switch (explorer->sort) {
			case LV_EXPLORER_SORT_NONE:
				break;
			case LV_EXPLORER_SORT_KIND:
				sort_by_file_kind(explorer->file_table, 0, (sum - 1));
				break;
			default:
				break;
		}
	}
}

/*Quick sort 3 way*/
static void sort_by_file_kind(lv_obj_t* tb, int16_t lo, int16_t hi) {
	if (lo >= hi) {
		return;
	}

	int16_t lt = lo;
	int16_t i = lo + 1;
	int16_t gt = hi;
	const char* v = lv_table_get_cell_value(tb, lo, 1);
	while (i <= gt) {
		if (strcmp(lv_table_get_cell_value(tb, i, 1), v) < 0) {
			exch_table_item(tb, lt++, i++);
		} else if (strcmp(lv_table_get_cell_value(tb, i, 1), v) > 0) {
			exch_table_item(tb, i, gt--);
		} else {
			i++;
		}
	}

	sort_by_file_kind(tb, lo, lt - 1);
	sort_by_file_kind(tb, gt + 1, hi);
}

bool is_end_with(const char* str1, const char* str2) {
	if (str1 == NULL || str2 == NULL) {
		return false;
	}

	uint16_t len1 = strlen(str1);
	uint16_t len2 = strlen(str2);
	if ((len1 < len2) || (len1 == 0 || len2 == 0)) {
		return false;
	}

	while (len2 >= 1) {
		if (str2[len2 - 1] != str1[len1 - 1]) {
			return false;
		}

		len2--;
		len1--;
	}

	return true;
}
