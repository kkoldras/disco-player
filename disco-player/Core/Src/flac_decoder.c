/*
 * flac_decoder.c
 *
 *  Created on: Sep 28, 2023
 *      Author: Karol
 */

#include <flac_decoder.h>
#include "SEGGER_SYSVIEW.h"

// #define FLAC_DECODER(x) printf x
#define FLAC_DECODER(x) while (0) {}
//#define FLAC_DECODER(x) SEGGER_SYSVIEW_PrintfTarget x


#define PREALLOC_SIZE 20480

static FLAC__StreamDecoder* decoder = NULL;

typedef struct {
	uint8_t* buffer;
	uint32_t maxSize;
	uint32_t filledSize;
} DecodedFrame;

typedef struct {
	FF_FILE* file;
	Audio_InfoTypedef* info;
	DecodedFrame* frame;
	uint32_t framePos;
	bool eof;
	bool error;
} ClientData;

static ClientData* currentData;

static void free_current_data(ClientData* data) {
	free(data->frame->buffer);
	data->frame->maxSize = 0;
	data->frame->filledSize = 0;
	free(data->frame);
	free(data);
}

static FLAC__StreamDecoderReadStatus read_callback(
	const FLAC__StreamDecoder* decoder,
	FLAC__byte buffer[],
	size_t* bytes,
	void* client_data
) {
	uint32_t t = xTaskGetTickCount();
	if (*bytes > 0) {
		ClientData* data = (ClientData*)client_data;
		FF_FILE* file = data->file;
		*bytes = ff_fread(buffer, 1, *bytes, file);
		if (*bytes > 0) {
			t = xTaskGetTickCount() - t;
			FLAC_DECODER(("Read cb(OK): %d ticks\n", t));
			return FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
		} else if (ff_feof(file)) {
			data->eof = true;
			t = xTaskGetTickCount() - t;
			FLAC_DECODER(("Read cb(EOF): %d ticks\n", t));
			return FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM;
		}
	}
	t = xTaskGetTickCount() - t;
	FLAC_DECODER(("Read cb(ERR): %d ticks\n", t));
	return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
}

static FLAC__StreamDecoderSeekStatus seek_callback(
	const FLAC__StreamDecoder* decoder,
	FLAC__uint64 absolute_byte_offset,
	void* client_data
) {
	ClientData* data = (ClientData*)client_data;
	uint32_t t = xTaskGetTickCount();
	FF_FILE* file = data->file;

	if (ff_fseek(file, (long)absolute_byte_offset, SEEK_SET) < 0) {
		t = xTaskGetTickCount() - t;
		FLAC_DECODER(("Seek cb: %d ticks\n", t));
		return FLAC__STREAM_DECODER_SEEK_STATUS_ERROR;
	} else {
		t = xTaskGetTickCount() - t;
		FLAC_DECODER(("Seek cb: %d ticks\n", t));
		return FLAC__STREAM_DECODER_SEEK_STATUS_OK;
	}
}

static FLAC__StreamDecoderTellStatus tell_callback(
	const FLAC__StreamDecoder* decoder,
	FLAC__uint64* absolute_byte_offset,
	void* client_data
) {
	ClientData* data = (ClientData*)client_data;
	uint32_t t = xTaskGetTickCount();
	FF_FILE* file = data->file;
	long pos = ff_ftell(file);
	t = xTaskGetTickCount() - t;
	FLAC_DECODER(("Tell cb: %d ticks\n", t));

	if (pos < 0) {
		return FLAC__STREAM_DECODER_TELL_STATUS_ERROR;
	} else {
		*absolute_byte_offset = (FLAC__uint64)pos;
		return FLAC__STREAM_DECODER_TELL_STATUS_OK;
	}
}

static FLAC__StreamDecoderLengthStatus length_callback(
	const FLAC__StreamDecoder* decoder,
	FLAC__uint64* stream_length,
	void* client_data
) {
	ClientData* data = (ClientData*)client_data;
	uint32_t t = xTaskGetTickCount();
	FF_FILE* file = data->file;
	size_t len = ff_filelength(file);

	t = xTaskGetTickCount() - t;
	FLAC_DECODER(("Length cb: %d ticks\n", t));
	if (len == 0) {
		return FLAC__STREAM_DECODER_LENGTH_STATUS_ERROR;
	} else {
		*stream_length = (FLAC__uint64)len;
		return FLAC__STREAM_DECODER_LENGTH_STATUS_OK;
	}
}

static FLAC__bool eof_callback(const FLAC__StreamDecoder* decoder, void* client_data) {
	uint32_t t = xTaskGetTickCount();
	ClientData* data = (ClientData*)client_data;
	bool res = ff_feof(data->file);
	data->eof = res;
	t = xTaskGetTickCount() - t;
	FLAC_DECODER(("EOF cb: %d ticks\n", t));
	return res;
}

static FLAC__StreamDecoderWriteStatus write_callback(
	const FLAC__StreamDecoder* decoder,
	const FLAC__Frame* frame,
	const FLAC__int32* const buffer[],
	void* client_data
) {
	ClientData* data = (ClientData*)client_data;
	uint32_t t = xTaskGetTickCount();
	for (int i = 0; i < frame->header.channels; i++) {
		if (buffer[i] == NULL) {
			SEGGER_SYSVIEW_PrintfHost("FLAC: buffer[%d] is NULL in write callback\n", i);
			return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
		}
	}

	int samples = frame->header.blocksize;
	int channels = frame->header.channels;
	int sampleSize = frame->header.bits_per_sample / 8;
	int size = samples * channels * sampleSize;

	if (size > data->frame->maxSize) {
		data->frame->buffer = realloc(data->frame->buffer, size);
		data->frame->maxSize = size;

		if (data->frame->buffer == NULL) {
			FLAC_DECODER(
				("write cb: Could not reallocate buffer of size %d. Heap left: %d\n",
			   size,
			   xPortGetFreeHeapSize())
			);
			return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
		} else {
			FLAC_DECODER(("write cb: Reallocated buffer %d, Heap left: %d\n", size, xPortGetFreeHeapSize()));
		}
	}

	uint32_t t1 = xTaskGetTickCount();
	for (int sample = 0; sample < samples; sample++) {
		for (int channel = 0; channel < channels; channel++) {
			for (int byte = 0; byte < sampleSize; byte++) {
				data->frame->buffer[(sample * channels + channel) * sampleSize + byte] =
					(uint8_t)((buffer[channel][sample] >> (byte * 8)) & 0xFF);
			}
		}
	}
	data->frame->filledSize = size;
	t1 = xTaskGetTickCount() - t1;
	t = xTaskGetTickCount() - t;
	FLAC_DECODER(("Write cb: %d ticks total, %d ticks to copy\n", t, t1));
	return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

static void metadata_callback(
	const FLAC__StreamDecoder* decoder,
	const FLAC__StreamMetadata* metadata,
	void* client_data
) {
	ClientData* data = (ClientData*)client_data;
	if (data->info && metadata->type == FLAC__METADATA_TYPE_STREAMINFO) {
		uint32_t duration =
			metadata->data.stream_info.total_samples / metadata->data.stream_info.sample_rate;
		data->info->Duration = duration;
		data->info->SampleRate = metadata->data.stream_info.sample_rate;
		data->info->SampleSize = metadata->data.stream_info.bits_per_sample;
	}
}

static void error_callback(
	const FLAC__StreamDecoder* decoder,
	FLAC__StreamDecoderErrorStatus status,
	void* client_data
) {
	ClientData* data = (ClientData*)client_data;
	data->error = true;
	SEGGER_SYSVIEW_PrintfHost("FLAC decoder error: %d\n", status);
}

static size_t flac_read_file(void* dstBuffer, size_t size, FF_FILE* file) {
	if (currentData != NULL && decoder != NULL) {
		size_t readData = 0;

		while (readData < size) {
			if (currentData->frame->filledSize == 0) {
				currentData->framePos = 0;
				uint32_t t = xTaskGetTickCount();

				bool res = FLAC__stream_decoder_process_single(decoder);
				if (!res || currentData->frame->filledSize == 0) {
					FLAC_DECODER(("Could not decode a new frame, returning %d bytes\n", readData));
					return readData;
				}

				t = xTaskGetTickCount() - t;
				FLAC_DECODER(("Decoded a new frame in %d ticks\n", t));
			}

			int leftInFrame = currentData->frame->filledSize - currentData->framePos;
			int spaceInDest = size - readData;

			if (leftInFrame <= spaceInDest) {
				memcpy(
					dstBuffer + readData,
					&currentData->frame->buffer[currentData->framePos],
					leftInFrame
				);
				FLAC_DECODER(
					("Finished reading a frame from %d to %d\n",
				   currentData->framePos,
				   currentData->framePos + leftInFrame)
				);
				readData += leftInFrame;
				currentData->frame->filledSize = 0;
				currentData->framePos = 0;
			} else {
				memcpy(
					dstBuffer + readData,
					&currentData->frame->buffer[currentData->framePos],
					spaceInDest
				);
				FLAC_DECODER(
					("Read data from %d to %d\n", currentData->framePos, currentData->framePos + spaceInDest)
				);
				readData += spaceInDest;
				currentData->framePos += spaceInDest;
			}
		}
		if (currentData->eof) {
			free_current_data(currentData);
			currentData = NULL;
		}
		FLAC_DECODER(("Returning %d bytes\n", readData));

		return readData;
	}

	return 0;
}

static uint8_t flac_set_position(FF_FILE* file, uint8_t percentage) { return 0; }

static uint8_t flac_get_file_info(FF_FILE* file, Audio_InfoTypedef* info) {
	ClientData data = {.file = file, .info = info, .frame = NULL, .eof = false, .error = false};
	long tmpPos = ff_ftell(file);
	if (ff_fseek(file, 0, FF_SEEK_SET) != 0) {
		return 0;
	}

	uint32_t magic = 0;
	size_t res = ff_fread(&magic, sizeof(uint32_t), 1, file);
	if (!res || magic != 0x43614c66) {
		return 0;
	}

	if (ff_fseek(file, 0, FF_SEEK_SET) != 0) {
		return 0;
	}


	if (decoder == NULL) {
		decoder = FLAC__stream_decoder_new();
		FLAC__stream_decoder_set_md5_checking(decoder, true);
	}

	FLAC__stream_decoder_init_stream(
		decoder,
		read_callback,
		seek_callback,
		tell_callback,
		length_callback,
		eof_callback,
		write_callback,
		metadata_callback,
		error_callback,
		&data
	);

	FLAC__stream_decoder_process_until_end_of_metadata(decoder);
	FLAC__stream_decoder_finish(decoder);

	if (ff_fseek(file, tmpPos, FF_SEEK_SET) != 0) {
		return 0;
	}

	if (data.error) {
		return 0;
	}

	return 1;
}

static void flac_init(FF_FILE* file) {
	if (decoder == NULL) {
		decoder = FLAC__stream_decoder_new();
		FLAC__stream_decoder_set_md5_checking(decoder, true);
	}

	if (FLAC__stream_decoder_get_state(decoder) != FLAC__STREAM_DECODER_UNINITIALIZED) {
		FLAC__stream_decoder_finish(decoder);
	}

	if (currentData != NULL) {
		free_current_data(currentData);
		currentData = NULL;
	}

	currentData = malloc(sizeof(ClientData));
	currentData->file = file;
	currentData->info = NULL;
	currentData->frame = malloc(sizeof(DecodedFrame));
	//prealloc big buffer for decoding. will be expanded if needed.
	currentData->frame->buffer = malloc(PREALLOC_SIZE);
	currentData->frame->maxSize = PREALLOC_SIZE;
	currentData->frame->filledSize = 0;
	currentData->eof = false;
	currentData->error = false;

	FLAC__stream_decoder_init_stream(
		decoder,
		read_callback,
		seek_callback,
		tell_callback,
		length_callback,
		eof_callback,
		write_callback,
		metadata_callback,
		error_callback,
		currentData
	);

	FLAC__stream_decoder_process_until_end_of_metadata(decoder);
}

static void flac_deinit(void) {
	if (currentData != NULL) {
		free_current_data(currentData);
		currentData = NULL;
	}

	if (decoder != NULL) {
		FLAC__stream_decoder_finish(decoder);
		FLAC__stream_decoder_delete(decoder);
		decoder = NULL;
	}
}

AudioDecoder flac_decoder = {
	.get_file_info_fn = flac_get_file_info,
	.read_file_fn = flac_read_file,
	.set_position_fn = flac_set_position,
	.init_fn = flac_init,
	.deinit_fn = flac_deinit
};
